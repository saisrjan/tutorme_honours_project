from rest_framework import serializers
from django.utils import timezone

from . import models


class PersonSerializer(serializers.Serializer):
    email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    first_name = serializers.CharField(required=True, allow_blank=False, max_length=50)
    last_name = serializers.CharField(required=True, allow_blank=False, max_length=50)
    age = serializers.IntegerField(required=True, allow_null=False)
    profile_picture_directory = serializers.CharField(allow_blank=True, max_length=100)
    location = serializers.CharField(allow_blank=False, max_length=100)

    def create(self, validated_data):
        return models.Person.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.age = validated_data.get('age', instance.age)
        instance.profile_picture_directory = validated_data.get('profile_picture_directory',
                                                                instance.profile_picture_directory)
        instance.location = validated_data.get('location')
        instance.save()
        return instance


class StudentSerializer(serializers.Serializer):
    email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    current_school = serializers.CharField(required=True, allow_blank=False, max_length=50)
    overall_rating_out_of_five = serializers.IntegerField(required=False)

    def create(self, validated_data):
        email = validated_data.get('email')
        current_school = validated_data.get('current_school')
        return models.Student.objects.create(email=email, current_school=current_school,
                                             overall_rating_out_of_five=5)

    def update(self, instance, validated_data):
        instance.current_school = validated_data.get('current_school', instance.current_school)
        instance.save()
        return instance


class TutorSerializer(serializers.Serializer):
    email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    date_joined = serializers.DateField(required=False)
    overall_rating_out_of_five = serializers.IntegerField(required=False)
    price = serializers.IntegerField(required=True, allow_null=False)

    def create(self, validated_data):
        email = validated_data.get('email')
        price = validated_data.get('price')
        return models.Tutor.objects.create(email=email, date_joined=timezone.now().strftime("%Y-%m-%d"),
                                           overall_rating_out_of_five=5, price=price)

    def update(self, instance, validated_data):
        instance.price = validated_data.get('price', instance.price)
        instance.save()
        return instance


class CourseSerializer(serializers.Serializer):
    email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    code = serializers.CharField(required=True, allow_blank=False, max_length=50)
    name = serializers.CharField(required=True, allow_blank=False, max_length=50)
    description = serializers.CharField(required=True, allow_blank=False, max_length=1000)
    school = serializers.CharField(required=True, allow_blank=False, max_length=50)
    grade = serializers.CharField(required=True, allow_blank=False, max_length=2)

    def create(self, validated_data):
        return models.Course.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.code = validated_data.get('code', instance.code)
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.school = validated_data.get('school', instance.school)
        instance.grade = validated_data.get('grade', instance.grade)
        instance.save()
        return instance


class AppointmentSerializer(serializers.Serializer):
    student_email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    tutor_email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    course_code = serializers.CharField(required=True, allow_blank=False, max_length=50)
    date = serializers.DateField(required=True, allow_null=False)
    time = serializers.CharField(required=True, allow_blank=False, max_length=5)
    duration = serializers.IntegerField(required=True, allow_null=False)
    price = serializers.IntegerField(required=True, allow_null=False)
    location = serializers.CharField(required=True, allow_blank=False, max_length=100)
    accepted = serializers.BooleanField(required=True, allow_null=False)
    declined = serializers.BooleanField(required=True, allow_null=False)

    def create(self, validated_data):
        return models.Appointment.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.date = validated_data.get('date', instance.date)
        instance.time = validated_data.get('time', instance.time)
        instance.duration = validated_data.get('duration', instance.duration)
        instance.price = validated_data.get('price', instance.price)
        instance.location = validated_data.get('location', instance.location)
        instance.accepted = validated_data.get('accepted', instance.accepted)
        instance.declined = validated_data.get('declined', instance.declined)
        instance.save()
        return instance


class ReviewSerializer(serializers.Serializer):
    reviewer_email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    reviewee_email = serializers.CharField(required=True, allow_blank=False, max_length=50)
    course_code = serializers.CharField(required=True, allow_blank=False, max_length=50)
    rating_out_of_five = serializers.IntegerField(required=True, allow_null=False)
    review_text = serializers.CharField(required=True, allow_blank=False, max_length=250)

    def create(self, validated_data):
        return models.Review.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.reviewer_email = validated_data.get('reviewer_email', instance.reviewer_email)
        instance.reviewee_email = validated_data.get('reviewee_email', instance.reviewee_email)
        instance.course_code = validated_data.get('course_code', instance.course_code)
        instance.rating_out_of_five = validated_data.get('rating_out_of_five', instance.rating_out_of_five)
        instance.review_text = validated_data.get('review_text', instance.review_text)

        instance.save()
        return instance


def serialize_student(student, person):
    # serialize tutor model
    serializer = StudentSerializer(student)

    student_json = serializer.data

    # serialize person model
    serializer = PersonSerializer(person)

    student_json['person'] = serializer.data

    return student_json


def serialize_tutor(tutor, person):
    # serialize tutor model
    serializer = TutorSerializer(tutor)

    tutor_json = serializer.data

    # serialize person model
    serializer = PersonSerializer(person)

    tutor_json['person'] = serializer.data

    return tutor_json


def serialize_course(course, tutor, person):
    tutor_json = serialize_tutor(tutor, person)

    # serialize course model
    serializer = CourseSerializer(course)

    course_json = serializer.data
    course_json['tutor'] = tutor_json

    return course_json


def serialize_appointment(appointment, tutor, tutor_person, student, student_person, course):
    serializer = AppointmentSerializer(appointment)

    appointment_json = serializer.data
    appointment_json['student'] = serialize_student(student, student_person)
    appointment_json['tutor'] = serialize_tutor(tutor, tutor_person)
    appointment_json['course'] = serialize_course(course, tutor, tutor_person)

    return appointment_json


def serialize_review(review, course, tutor, reviewee_person, reviewer_person):
    serializer = ReviewSerializer(review)

    review_json = serializer.data
    review_json['reviewee_person'] = PersonSerializer(reviewee_person).data
    review_json['reviewer_person'] = PersonSerializer(reviewer_person).data
    review_json['course'] = serialize_course(course, tutor, reviewee_person)

    return review_json

