from django.http import JsonResponse
from django.shortcuts import redirect
from rest_framework.decorators import api_view
from django.db.models import Q

from . import models
from . import serializers
from . import response_methods


@api_view(['GET'])
def server_status(request):
    if request.method == 'GET':
        return JsonResponse({'status': 'online'})


@api_view(['GET'])
def index(request):
    if request.method == 'GET':
        return redirect('server_status')


@api_view(['POST'])
def add_course(request):
    if request.method == 'POST':
        data = request.POST.dict()
        email_req = data.get('email', '')
        code_req = data.get('code', '')

        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        course = models.Course.objects.filter(email=email_req, code=code_req).first()
        if course is not None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        serializer = serializers.CourseSerializer(data=data)

        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def update_course(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')
        code_req = data.get('code', '')

        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        course = models.Course.objects.filter(email=email_req, code=code_req).first()
        if course is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        serializer = serializers.CourseSerializer(course, data=data)

        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def delete_course(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')
        code_req = data.get('code', '')

        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_http_response(response_methods.NOT_FOUND_STATUS, "Tutor not found")

        course = models.Course.objects.filter(email=email_req, code=code_req).first()
        if course is None:
            return response_methods.get_http_response(response_methods.NOT_FOUND_STATUS, "Course not found")

        course.delete()
        return response_methods.get_http_response(response_methods.DELETED_STATUS, "Deleted")


@api_view(['GET'])
def get_courses(request):
    if request.method == 'GET':
        data = request.GET.dict()

        email_req = data.get('email')

        courses_completed = models.Course.objects.filter(email=email_req)

        course_json_list = []
        for course in courses_completed:
            tutor = models.Tutor.objects.filter(email=course.email).first()
            person = models.Person.objects.filter(email=course.email).first()

            if tutor is None or person is None:
                return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

            course_json_list.append(serializers.serialize_course(course, tutor, person))

        return response_methods.get_response(response_methods.OK_STATUS, course_json_list)


@api_view(['POST'])
def create_appointment(request):
    if request.method == 'POST':
        data = request.POST.dict()

        tutor_email_req = data.get('tutor_email')
        student_email_req = data.get('student_email')

        tutor = models.Tutor.objects.filter(email=tutor_email_req).first()
        if tutor is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        student = models.Student.objects.filter(email=student_email_req).first()
        if student is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        serializer = serializers.AppointmentSerializer(data=data)

        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def update_appointment(request):
    if request.method == 'POST':
        data = request.POST.dict()

        tutor_email_req = data.get('tutor_email')
        student_email_req = data.get('student_email')

        appointment = models.Appointment.objects.filter(student_email=student_email_req,
                                                        tutor_email=tutor_email_req).first()
        if appointment is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        serializer = serializers.AppointmentSerializer(appointment, data=data)

        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def delete_appointment(request):
    if request.method == 'POST':
        data = request.POST.dict()

        tutor_email_req = data.get('tutor_email')
        student_email_req = data.get('student_email')

        appointment = models.Appointment.objects.filter(student_email=student_email_req,
                                                        tutor_email=tutor_email_req).first()
        if appointment is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        appointment.delete()
        return response_methods.get_response(response_methods.UPDATED_STATUS, data)


@api_view(['GET'])
def get_appointments_for_student(request):
    if request.method == 'GET':
        data = request.GET.dict()

        student_email_req = data.get('email')

        appointments = models.Appointment.objects.filter(student_email=student_email_req)

        appointments_json_list = []
        for appointment in appointments:
            tutor = models.Tutor.objects.filter(email=appointment.tutor_email).first()
            tutor_person = models.Person.objects.filter(email=appointment.tutor_email).first()
            student = models.Student.objects.filter(email=appointment.student_email).first()
            student_person = models.Person.objects.filter(email=appointment.student_email).first()
            course = models.Course.objects.filter(email=appointment.tutor_email, code=appointment.course_code).first()

            if tutor is None or tutor_person is None or student is None or student_person is None or course is None:
                return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

            appointments_json_list.append(serializers.serialize_appointment(appointment, tutor, tutor_person, student,
                                                                            student_person, course))

        return response_methods.get_response(response_methods.OK_STATUS, appointments_json_list)


@api_view(['GET'])
def get_appointments_for_tutor(request):
    if request.method == 'GET':
        data = request.GET.dict()

        tutor_email_req = data.get('email')

        appointments = models.Appointment.objects.filter(tutor_email=tutor_email_req)

        appointments_json_list = []
        for appointment in appointments:
            tutor = models.Tutor.objects.filter(email=appointment.tutor_email).first()
            tutor_person = models.Person.objects.filter(email=appointment.tutor_email).first()
            student = models.Student.objects.filter(email=appointment.student_email).first()
            student_person = models.Person.objects.filter(email=appointment.student_email).first()
            course = models.Course.objects.filter(email=appointment.tutor_email, code=appointment.course_code).first()

            if tutor is None or tutor_person is None or student is None or student_person is None or course is None:
                return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

            appointments_json_list.append(serializers.serialize_appointment(appointment, tutor, tutor_person, student,
                                                                            student_person, course))

        return response_methods.get_response(response_methods.OK_STATUS, appointments_json_list)


@api_view(['POST'])
def create_review(request):
    """
    Create review of a user
    :param request: request object containing the review information
    :return: http response (status, message)
    """
    if request.method == 'POST':
        data = request.POST.dict()

        reviewer_email_req = data.get('reviewer_email')
        reviewee_email_req = data.get('reviewee_email')
        course_code_req = data.get('course_code')

        # search if the users associated with the review exist
        person_1 = models.Person.objects.filter(email=reviewer_email_req).first()
        if person_1 is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        peron_2 = models.Person.objects.filter(email=reviewee_email_req).first()
        if peron_2 is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        review = models.Review.objects.filter(reviewee_email=reviewee_email_req,
                                              reviewer_email=reviewer_email_req,
                                              course_code=course_code_req).first()
        if review is not None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        # create review
        serializer = serializers.ReviewSerializer(data=data)

        if not serializer.is_valid():
            return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_http_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def delete_review(request):
    """
    Delete review of a user
    :param request: request object containing the review information
    :return: http response (status, message)
    """
    if request.method == 'POST':
        data = request.POST.dict()

        reviewer_email_req = data.get('reviewer_email')
        reviewee_email_req = data.get('reviewee_email')

        # get review from database associated with the emails
        review = models.Review.objects.filter(reviewer_email=reviewer_email_req, reviewee_email=reviewee_email_req)
        if review is None:
            return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Not Found")

        review.delete()
        return response_methods.get_http_response(response_methods.UPDATED_STATUS, "Deleted")


@api_view(['GET'])
def get_reviews(request):
    """
    Get reviews of a user
    :param request: request object containing the tutor information
    :return: json response list of review
    """
    if request.method == 'GET':
        data = request.GET.dict()

        tutor_email_req = data.get('email')

        # get review from database associated with the emails
        reviews = models.Review.objects.filter(reviewee_email=tutor_email_req)
        review_list = []
        for review in reviews:
            person_tutor = models.Person.objects.filter(email=review.reviewee_email).first()
            if person_tutor is None:
                return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Not Found")

            person_student = models.Person.objects.filter(email=review.reviewer_email).first()
            if person_student is None:
                return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Not Found")

            tutor = models.Tutor.objects.filter(email=review.reviewee_email).first()
            if tutor is None:
                return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Not Found")

            course = models.Course.objects.filter(email=review.reviewee_email).first()
            if course is None:
                return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Not Found")

            review_list.append(serializers.serialize_review(review, course, tutor, person_tutor, person_student))

        return response_methods.get_response(response_methods.OK_STATUS, review_list)


def _search_tutors(location, query_arr):
    """
    Searches Person, Course and Tutor tables for matching instances of the query keywords
    Searches Person, Course and Tutor tables for matching instances of the query keywords
    :param location: location of the client
    :param query_arr: list of query keywords
    :return: returns list of distinct tutors
    """
    list_of_tutors = models.Tutor.objects.none()
    list_of_persons = models.Person.objects.none()
    list_of_courses = models.Course.objects.none()

    persons = models.Person.objects.filter(location__icontains=location)

    # get list of persons where query keywords match one or more of its attributes
    for query in query_arr:
        list_of_persons |= persons.filter(Q(email__icontains=query) | Q(first_name__icontains=query) |
                                              Q(last_name__icontains=query) | Q(location__icontains=query)).distinct()
    # get list with distinct elements
    list_of_persons = list_of_persons.distinct()

    # get list of courses where query keywords match one or more of its attributes
    for query in query_arr:
        list_of_courses |= models.Course.objects.filter(Q(code__icontains=query) | Q(name__icontains=query) |
                                                        Q(description__icontains=query) |
                                                        Q(school__icontains=query)).distinct()
    # get list with distinct elements
    list_of_courses = list_of_courses.distinct()

    # populate tutor list that is associated with the matching person or course
    for person in list_of_persons:
        list_of_tutors |= models.Tutor.objects.filter(email=person.email)

    for course in list_of_courses:
        list_of_tutors |= models.Tutor.objects.filter(email=course.email)

    # get list with distinct elements
    list_of_tutors = list_of_tutors.distinct()

    return list_of_tutors


@api_view(['GET'])
def search_tutors(request):
    """
    Get list of tutors that match the query sent
    :param request: request object containing the query sent from the client
    :return: list of tutors
    """
    if request.method == 'GET':
        data = request.GET.dict()

        email_req = data.get('email', '')

        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        query_req = data.get('query', '')

        query_req_arr = query_req.split(' ')

        tutor_list = []
        tutor_search_results = _search_tutors(person.location, query_req_arr)
        for tutor in tutor_search_results:
            person = models.Person.objects.filter(email=tutor.email).first()
            if person is not None:
                tutor_list.append(serializers.serialize_tutor(tutor, person))

        return response_methods.get_response(response_methods.OK_STATUS, tutor_list)




