from django.test import Client
from django.test import TestCase

from . import models


class PersonTestCase(TestCase):
    c = Client()

    '''
        Test for creating a person object
    '''
    def test_create_person_1(self):
        email = 'test@gmail.com'
        first_name = 'Test'
        last_name = 'Test'
        age = 24
        profile_picture_directory = 'test_directory'
        location = 'Ottawa'

        data = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        person = models.Person.objects.filter(email=email).first()

        self.assertEquals(email, getattr(person, 'email'))
        self.assertEquals(first_name, getattr(person, 'first_name'))
        self.assertEquals(last_name, getattr(person, 'last_name'))
        self.assertEquals(age, getattr(person, 'age'))
        self.assertEquals(profile_picture_directory, getattr(person, 'profile_picture_directory'))
        self.assertEquals(location, getattr(person, 'location'))
        
    '''
        Test for creating a person with no email
    '''
    def test_create_person_2(self):
        data = {'email': '', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with no first name
    '''
    def test_create_person_3(self):
        data = {'email': 'test@gmail.com', 'password': 'password', 'first_name': '', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with no last name
    '''
    def test_create_person_4(self):
        data = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': '', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with no age
    '''
    def test_create_person_5(self):
        data = {'email': 'test@gmail.com', 'password': 'password', 'first_name': '', 'last_name': 'Test', 'age': None,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with no location
    '''
    def test_create_person_6(self):
        data = {'email': 'test@gmail.com', 'password': 'password', 'first_name': '', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': ''}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with no test directory
    '''
    def test_create_person_7(self):
        data = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': '', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals('test@gmail.com', getattr(person, 'email'))

    '''
        Test for creating a person with more than on missing parameters
    '''
    def test_create_person_8(self):
        data = {'email': 'test@gmail.com','password': 'password', 'first_name': '', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': '', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

        person = models.Person.objects.filter(email='test@gmail.com').first()

        self.assertEquals(None, person)

    '''
        Test for creating a person with an existing email address
    '''
    def test_create_person_9(self):
        data = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        res = self.c.post('/tutormewebapi/sign_up', data, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 409)

        self.assertEquals(1, models.Person.objects.filter(email='test@gmail.com').count())

    '''
        Test for updating person
    '''
    def test_update_person_1(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'first_name': 'First Name', 'last_name': 'Last Name', 'age': 25,
                  'profile_picture_directory': '', 'location': 'Toronto'}

        res = self.c.post('/tutormewebapi/update_person', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        self.assertEquals(1, models.Person.objects.filter(email='test@gmail.com').count())

        person = models.Person.objects.filter(email='test@gmail.com').first()
        self.assertEquals(True, person is not None)
        self.assertEquals('First Name', getattr(person, 'first_name'))
        self.assertEquals('Last Name', getattr(person, 'last_name'))
        self.assertEquals(25, getattr(person, 'age'))
        self.assertEquals('', getattr(person, 'profile_picture_directory'))
        self.assertEquals('Toronto', getattr(person, 'location'))

    '''
        Test for updating person with blank data
    '''
    def test_update_person_2(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'first_name': ' ', 'last_name': '', 'age': 25,
                  'profile_picture_directory': '', 'location': ''}

        res = self.c.post('/tutormewebapi/update_person', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    '''
        Test for updating person incorrect email address
    '''
    def test_update_person_3(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test2@gmail.com', 'first_name': ' ', 'last_name': '', 'age': 25,
                  'profile_picture_directory': '', 'location': ''}

        res = self.c.post('/tutormewebapi/update_person', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 404)

    '''
        Test for updating person with incorrect json
    '''
    def test_update_person_4(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'last_name': '', 'age': 25,
                  'profile_picture_directory': '', 'location': ''}

        res = self.c.post('/tutormewebapi/update_person', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)


class TutorTestCase(TestCase):
    c = Client()

    def test_create_tutor_1(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        tutor = models.Tutor.objects.filter(email='test@gmail.com').first()

        self.assertEquals(True, tutor is not None)

        self.assertEquals(24, getattr(tutor, 'price'))

    def test_create_tutor_2(self):
        data_1 = {'email': 'test@gmail.com','password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test2@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 404)

    def test_create_tutor_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11'}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_tutor_1(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 25}
        res = self.c.post('/tutormewebapi/update_tutor', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        tutor = models.Tutor.objects.filter(email='test@gmail.com').first()

        self.assertEquals(True, tutor is not None)
        self.assertEquals(25, getattr(tutor, 'price'))

    def test_update_tutor_2(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 'yg'}
        res = self.c.post('/tutormewebapi/update_tutor', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_tutor_3(self):
        data_1 = {'email': 'test@gmail.com','password': 'password',  'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com'}
        res = self.c.post('/tutormewebapi/update_tutor', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_tutor_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price-1': 24}
        res = self.c.post('/tutormewebapi/update_tutor', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)


class StudentTestCase(TestCase):
    c = Client()

    def test_create_student_1(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'current_school': 'Carleton University'}
        res = self.c.post('/tutormewebapi/create_student', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        student = models.Student.objects.filter(email='test@gmail.com').first()

        self.assertEquals(True, student is not None)
        self.assertEquals('Carleton University', getattr(student, 'current_school'))

    def test_create_student_2(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test2@gmail.com', 'current_school': 'Carleton University'}
        res = self.c.post('/tutormewebapi/create_student', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 409)

    def test_create_student_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'current_school': ''}
        res = self.c.post('/tutormewebapi/create_student', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_student_1(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'current_school': 'Carleton University'}
        res = self.c.post('/tutormewebapi/create_student', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'current_school': 'Western University'}
        res = self.c.post('/tutormewebapi/update_student', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        student = models.Student.objects.filter(email='test@gmail.com').first()

        self.assertEquals(True, student is not None)
        self.assertEquals('Western University', getattr(student, 'current_school'))

    def test_update_student_2(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'current_school': 'Western University'}
        res = self.c.post('/tutormewebapi/update_student', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 404)

    def test_update_student_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'current_school': 'Carleton University'}
        res = self.c.post('/tutormewebapi/create_student', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com'}
        res = self.c.post('/tutormewebapi/update_student', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)


class CourseTestCase(TestCase):
    c = Client()

    def test_add_course_1(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        course = models.Course.objects.filter(email='test@gmail.com', code="Test").first()

        self.assertEquals(True, course is not None)
        self.assertEquals('Test', getattr(course, 'code'))
        self.assertEquals('Test', getattr(course, 'name'))
        self.assertEquals('Example', getattr(course, 'description'))
        self.assertEquals('Carleton University', getattr(course, 'school'))
        self.assertEquals('A+', getattr(course, 'grade'))

    def test_add_course_2(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test2@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 404)

    def test_add_course_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': '', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_add_course_4(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': '', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_add_course_5(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': '',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_add_course_6(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': '', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_add_course_7(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test', 'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton Univerisity', 'grade': ''}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_course_1(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test',
                  'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_4 = {'email': 'test@gmail.com', 'code': 'Test1', 'name': 'Test1', 'description': 'Example1',
                  'school': 'U of Ottawa1', 'grade': 'B'}
        res = self.c.post('/tutormewebapi/add_course', data_4, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

    def test_update_course_2(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test',
                  'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_4 = {'email': 'test@gmail.com', 'co3de': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'U of Ottawa', 'grade': 'A'}
        res = self.c.post('/tutormewebapi/add_course', data_4, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)

    def test_update_course_3(self):
        data_1 = {'email': 'test@gmail.com', 'password': 'password', 'first_name': 'Test', 'last_name': 'Test',
                  'age': 24,
                  'profile_picture_directory': 'test_directory', 'location': 'Ottawa'}

        res = self.c.post('/tutormewebapi/sign_up', data_1, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_2 = {'email': 'test@gmail.com', 'date_joined': '2019-02-11', 'price': 24}
        res = self.c.post('/tutormewebapi/create_tutor', data_2, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_3 = {'email': 'test@gmail.com', 'code': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'Carleton University', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_3, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 201)

        data_4 = {'email': 'test@gmail.com', 'cofde': 'Test', 'name': 'Test', 'description': 'Example',
                  'school': 'U of Ottawa', 'grade': 'A+'}
        res = self.c.post('/tutormewebapi/add_course', data_4, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(res.status_code, 400)





