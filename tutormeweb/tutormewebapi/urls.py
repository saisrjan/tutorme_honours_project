from django.urls import path

from . import account_views
from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('server_status', views.server_status, name='server_status'),
    path('sign_up', account_views.sign_up, name='sign_up'),
    path('update_person', account_views.update_person, name='update_person'),
    path('create_tutor', account_views.create_tutor, name='create_tutor'),
    path('create_student', account_views.create_student, name='create_student'),
    path('update_tutor', account_views.update_tutor, name='update_tutor'),
    path('update_student', account_views.update_student, name='update_student'),
    path('add_course', views.add_course, name='add_course'),
    path('update_course', views.update_course, name='update_course'),
    path('delete_course', views.delete_course, name='delete_course'),
    path('get_courses', views.get_courses, name='get_courses'),
    path('create_review', views.create_review, name='create_review'),
    path('delete_review', views.delete_review, name='delete_review'),
    path('tutor_login', account_views.tutor_login, name='tutor_login'),
    path('student_login', account_views.student_login, name='student_login'),
    path('get_tutor', account_views.get_tutor, name='get_tutor'),
    path('get_student', account_views.get_student, name='get_student'),
    path('logout_user', account_views.logout_user, name='logout_user'),
    path('search_tutors', views.search_tutors, name='search_tutors'),
    path('get_reviews', views.get_reviews, name='get_reviews'),
    path('create_appointment', views.create_appointment, name='create_appointment'),
    path('update_appointment', views.update_appointment, name='update_appointment'),
    path('delete_appointment', views.delete_appointment, name='delete_appointment'),
    path('get_appointments_for_student', views.get_appointments_for_student, name='get_appointments_for_student'),
    path('get_appointments_for_tutor', views.get_appointments_for_tutor, name='get_appointments_for_tutor'),
]