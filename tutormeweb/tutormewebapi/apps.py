from django.apps import AppConfig


class TutormewebapiConfig(AppConfig):
    name = 'tutormewebapi'
