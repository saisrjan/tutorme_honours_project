from django.db import models


class Person(models.Model):
    email = models.CharField(max_length=50, primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    age = models.PositiveIntegerField()
    profile_picture_directory = models.CharField(max_length=100)
    location = models.CharField(max_length=100)


class Student(models.Model):
    email = models.CharField(max_length=50, primary_key=True)
    current_school = models.CharField(max_length=50)
    overall_rating_out_of_five = models.IntegerField()


class Tutor(models.Model):
    email = models.CharField(max_length=50, primary_key=True)
    date_joined = models.DateField()
    overall_rating_out_of_five = models.IntegerField()
    price = models.PositiveIntegerField()


class Course(models.Model):
    email = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=1000)
    school = models.CharField(max_length=50)
    grade = models.CharField(max_length=2)

    class Meta:
        unique_together = ('email', 'code',)


class Review(models.Model):
    reviewer_email = models.CharField(max_length=50)
    reviewee_email = models.CharField(max_length=50)
    course_code = models.CharField(max_length=50)
    rating_out_of_five = models.IntegerField()
    review_text = models.CharField(max_length=250)

    class Meta:
        unique_together = ('reviewer_email', 'reviewee_email', 'course_code')


class Appointment(models.Model):
    student_email = models.CharField(max_length=50)
    tutor_email = models.CharField(max_length=50)
    course_code = models.CharField(max_length=50)
    date = models.DateField()
    time = models.CharField(max_length=5)
    duration = models.IntegerField()
    price = models.IntegerField()
    location = models.CharField(max_length=100)
    accepted = models.BooleanField()
    declined = models.BooleanField()

    class Meta:
        unique_together = ('student_email', 'tutor_email',)



