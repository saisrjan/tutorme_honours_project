from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from rest_framework.decorators import api_view

from . import models
from . import serializers
from . import response_methods


@api_view(['POST'])
def sign_up(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = request.POST.get('email', '')
        password_req = request.POST.get('password', '')

        person = models.Person.objects.filter(email=email_req).first()
        if person is not None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        # create person
        if 'password' in data:
            data.pop('password')
        else:
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, {})

        serializer = serializers.PersonSerializer(data=data)
        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()

        # create the new user
        user = User.objects.create_user(email_req, email_req, password_req)
        user.save()

        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def update_person(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')

        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        # create person
        serializer = serializers.PersonSerializer(person, data=data)
        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['GET'])
def get_person(request):
    """
    Gets person information with the email/username given

    :param request: request object containing the information received from the client
    :return: person information with the email/username given
    """
    if request.method == 'GET':
        data = request.GET.dict()

        email_req = data.get('email', '')

        # returns failed status if person does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        # serialize tutor model
        serializer = serializers.PersonSerializer(person)

        return response_methods.get_response(response_methods.OK_STATUS, serializer.data)


@api_view(['POST'])
def create_tutor(request):
    """
    Create tutor account

    :param request: request object
    :return:
    """
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')

        # returns failed status if user does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        # returns exists status if tutor exits
        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is not None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        data.pop('date_joined')
        serializer = serializers.TutorSerializer(data=data)
        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def update_tutor(request):
    """

    :param request:
    :return:
    """
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')

        # returns failed status if user does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        # returns exists status if tutor exits
        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        serializer = serializers.TutorSerializer(tutor, data=data)
        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['GET'])
def get_tutor(request):
    """
    Gets tutor information with the email/username given

    :param request: request object containing the information received from the client
    :return: tutor information with the email/username given
    """
    if request.method == 'GET':
        data = request.GET.dict()

        email_req = data.get('email', '')

        # returns failed status if tutor does not exist
        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        # returns failed status if person does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        return response_methods.get_response(response_methods.OK_STATUS, serializers.serialize_tutor(tutor, person))


@api_view(['POST'])
def create_student(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')

        # returns failed status if user does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        # returns exists status if student exits
        student = models.Student.objects.filter(email=email_req).first()
        if student is not None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        serializer = serializers.StudentSerializer(data=data)
        if not serializer.is_valid():
            print(serializer.errors)
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['POST'])
def update_student(request):
    if request.method == 'POST':
        data = request.POST.dict()

        email_req = data.get('email', '')

        # returns failed status if user does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.CONFLICT_STATUS, {})

        # returns exists status if student exits
        student = models.Student.objects.filter(email=email_req).first()
        if student is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        serializer = serializers.StudentSerializer(student, data=data)
        if not serializer.is_valid():
            return response_methods.get_response(response_methods.BAD_REQUEST_STATUS, serializer.errors)

        serializer.save()
        return response_methods.get_response(response_methods.UPDATED_STATUS, serializer.data)


@api_view(['GET'])
def get_student(request):
    """
    Gets student information with the email/username given

    :param request: request object containing the information received from the client
    :return: student information with the email/username given
    """
    if request.method == 'GET':
        data = request.GET.dict()

        email_req = data.get('email', '')

        # returns failed status if student does not exist
        student = models.Student.objects.filter(email=email_req).first()
        if student is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        # returns failed status if person does not exist
        person = models.Person.objects.filter(email=email_req).first()
        if person is None:
            return response_methods.get_response(response_methods.NOT_FOUND_STATUS, {})

        return response_methods.get_response(response_methods.OK_STATUS, serializers.serialize_student(student, person))


def _get_login_information(request):
    """
    Get login credentials from request

    :param request: request object containing the information received from the client
    :return: email and password received from the client
    """
    data = request.POST.dict()

    # get credentials from the request
    email_req = data.get('email', '')
    password_req = data.get('password', '')

    return email_req, password_req


def _login(request, email_req, password_req):
    """
    Authenticates the user with the given email and password from the http request

    :param request: request object containing the information received from the client
    :param email_req: email received from the http request
    :param password_req: password received from the http request
    :return: response object containing the information that should be sent to the client
    """


    # authenticate
    user = authenticate(request, username=email_req, password=password_req)
    if user is not None:
        # if a user exists associated with the credentials given return OK (200) status
        return response_methods.get_http_response(response_methods.OK_STATUS, "Login Successful")
    else:
        # if a user associated with the credentials does not exist return CONFLICTED (409) status
        return response_methods.get_http_response(response_methods.CONFLICT_STATUS, "Login Failed")


@api_view(['POST'])
def student_login(request):
    """
    Authenticates a student account with the given email and password from the http request

    :param request: request object containing the information received from the client
    :return: response object containing the information that should be sent to the client
    """

    if request.method == 'POST':
        email_req, password_req = _get_login_information(request)

        # missing information will return BAD_REQUEST (400) status
        if email_req == '' or password_req == '':
            return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Bad Request. Login Failed")

        # if a student associated with the credentials does not exist return CONFLICTED (409) status
        student = models.Student.objects.filter(email=email_req).first()
        if student is None:
            return response_methods.get_http_response(response_methods.CONFLICT_STATUS, "Login Failed")

        return _login(request, email_req, password_req)


@api_view(['POST'])
def tutor_login(request):
    """
    Authenticates a tutor account with the given email and password from the http request

    :param request: request object containing the information received from the client
    :return: response object containing the information that should be sent to the client
    """

    if request.method == 'POST':
        email_req, password_req = _get_login_information(request)

        # missing information will return BAD_REQUEST (400) status
        if email_req == '' or password_req == '':
            return response_methods.get_http_response(response_methods.BAD_REQUEST_STATUS, "Bad Request. Login Failed")

        # if a student associated with the credentials does not exist return CONFLICTED (409) status
        tutor = models.Tutor.objects.filter(email=email_req).first()
        if tutor is None:
            return response_methods.get_http_response(response_methods.CONFLICT_STATUS, "Login Failed")

        return _login(request, email_req, password_req)


@api_view(['POST'])
def logout_user(request):
    logout(request)

    return response_methods.get_http_response(response_methods.OK_STATUS, "Logout Successful")

