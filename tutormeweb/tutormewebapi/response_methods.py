from django.http import HttpResponse, JsonResponse

OK_STATUS = 200
UPDATED_STATUS = 201
DELETED_STATUS = 204
NOT_FOUND_STATUS = 404
BAD_REQUEST_STATUS = 400
CONFLICT_STATUS = 409


def get_response(status, data):
    json_response = {
        'status': status,
        'data': data
    }
    return JsonResponse(json_response, status=status, safe=False)


def get_http_response(status, message):
    return HttpResponse(message, status=status)
