package com.example.tutorme.corelib.serializers;

/**
 * SerializerKeys class contains all keys for JSON deserialization and
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class SerializerKeys {
    // appointment serializer keys
    public static final String STUDENT_EMAIL_KEY = "student_email";
    public static final String TUTOR_EMAIL_KEY = "tutor_email";
    public static final String COURSE_CODE_KEY = "course_code";
    public static final String DATE_KEY= "date";
    public static final String TIME_KEY = "time";
    public static final String DURATION_KEY = "duration";
    public static final String PRICE_KEY =  "price";
    public static final String ACCEPTED_KEY = "accepted";
    public static final String DECLINED_KEY = "declined";

    public static final String CODE_KEY = "code";
    public static final String NAME_KEY = "name";
    public static final String DESCRIPTION_KEY = "description";
    public static final String SCHOOL_KEY = "school";
    public static final String GRADE_KEY = "grade";

    public static final String EMAIL_KEY = "email";
    public static final String PASSWORD_KEY = "password";
    public static final String FIRST_NAME_KEY = "first_name";
    public static final String LAST_NAME_KEY = "last_name";
    public static final String AGE_KEY = "age";
    public static final String LOCATION_KEY = "location";
    public static final String PROFILE_PIC_DIRECTORY = "profile_picture_directory";

    public static final String REVIEWER_EMAIL_KEY = "reviewer_email";
    public static final String REVIEWEE_EMAIL_KEY = "reviewee_email";
    public static final String RATING_OUT_OF_FIVE_KEY = "rating_out_of_five";
    public static final String REVIEW_TEXT_KEY = "review_text";
    public static final String REVIEWER_PERSON_KEY = "reviewer_person";
    public static final String REVIEWEE_PERSON_KEY = "reviewee_person";

    public static final String DATE_JOINED = "date_joined";
    public static final String OVERALL_RATING_OUT_OF_FIVE = "overall_rating_out_of_five";

    public static final String CURRENT_SCHOOL = "current_school";

    public static final String PERSON_KEY = "person";
    public static final String STUDENT_KEY = "student";
    public static final String TUTOR_KEY = "tutor";
    public static final String COURSE_KEY = "course";
}
