package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Student;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Studenterializer class handler JSON deserialization and Student
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class StudentSerializer {
    /**
     * Converts a student object to a request param object containing the student information
     * @param student student object to convert
     * @return request param object containing student information
     */
    public static RequestParams serialize(Student student) {
        RequestParams personInformation = new RequestParams();
        personInformation.put(SerializerKeys.EMAIL_KEY, student.email);
        personInformation.put(SerializerKeys.CURRENT_SCHOOL, student.currentSchool);
        personInformation.put(SerializerKeys.RATING_OUT_OF_FIVE_KEY, student.overallRating);

        return personInformation;
    }

    /**
     * Converts a JSON object containing student information to a Student object
     * @param studentJSONObject JSON object containing student information
     * @return Student object containing student information
     */
    public static Student deserializer(JSONObject studentJSONObject) {
        Student student = null;

        try {
            String email = studentJSONObject.getString(SerializerKeys.EMAIL_KEY);
            String firstName = studentJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.FIRST_NAME_KEY);
            String lastName = studentJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.LAST_NAME_KEY);
            int age = studentJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getInt(SerializerKeys.AGE_KEY);
            String profilePictureDirectory = studentJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.PROFILE_PIC_DIRECTORY);
            String location = studentJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.LOCATION_KEY);
            String currentSchool = studentJSONObject.getString(SerializerKeys.CURRENT_SCHOOL);
            int overallRating = studentJSONObject.getInt(SerializerKeys.OVERALL_RATING_OUT_OF_FIVE);

            student = new Student(email, firstName, lastName, age, profilePictureDirectory, location,
                    currentSchool, overallRating);
        } catch (JSONException e) {
            Log.e("Person", e.getMessage());
        }

        return student;
    }
}
