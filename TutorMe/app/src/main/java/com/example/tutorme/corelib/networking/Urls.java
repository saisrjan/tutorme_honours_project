package com.example.tutorme.corelib.networking;

/**
 * Urls class contains all the REST API urls
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Urls {
    public static final String BASE_URL = "http://192.168.0.13:8000";
    public static final String REST_API_ROUTE = "tutormewebapi";

    public static String INDEX = "index";
    public static String SERVER_STATUS = "server_status";
    public static String SIGN_UP = "sign_up";
    public static String UPDATE_PERSON = "update_person";
    public static String CREATE_TUTOR = "create_tutor";
    public static String CREATE_STUDENT = "create_student";
    public static String UPDATE_TUTOR = "update_tutor";
    public static String UPDATE_STUDENT = "update_student";
    public static String ADD_COURSE = "add_course";
    public static String UPDATE_COURSE = "update_course";
    public static String DELETE_COURSE = "delete_course";
    public static String GET_COURSES = "get_courses";
    public static String CREATE_REVIEW = "create_review";
    public static String DELETE_REVIEW = "delete_review";
    public static String TUTOR_LOGIN = "tutor_login";
    public static String STUDENT_LOGIN = "student_login";
    public static String GET_TUTOR = "get_tutor";
    public static String GET_STUDENT = "get_student";
    public static String LOGOUT_USER = "logout_user";
    public static String SEARCH_TUTORS = "search_tutors";
    public static String GET_REVIEWS = "get_reviews";
    public static String CREATE_APPOINTMENT = "create_appointment";
    public static String UPDATE_APPOINTMENT = "update_appointment";
    public static String DELETE_APPOINTMENT = "delete_appointment";
    public static String GET_APPOINTMENT_FOR_STUDENT = "get_appointments_for_student";
    public static String GET_APPOINTMENT_FOR_TUTOR = "get_appointments_for_tutor";

    public static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + "/" + REST_API_ROUTE + "/" + relativeUrl;
    }
}
