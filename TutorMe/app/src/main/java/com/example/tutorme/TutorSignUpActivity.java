package com.example.tutorme;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.TutorSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

public class TutorSignUpActivity extends AppCompatActivity {
    public static String ERROR_TAG = "TutorSignUpActivity";

    private EditText priceEditText;
    private TextView errorMessageTextView;

    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_sign_up);

        Intent intent = getIntent();
        person = (Person) intent.getSerializableExtra(IntentKeys.PERSON_KEY);

        // inflate all activity components
        priceEditText = findViewById(R.id.text_input_price);
        errorMessageTextView = findViewById(R.id.text_tutor_sign_up_error);

        Button tutorSignUpButton = findViewById(R.id.button_sign_up_tutor);
        tutorSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpTutor();
            }
        });
    }

    /**
     * Sign up tutor.
     * Get tutor information from fields in the activity and creates tutor object
     */
    private void signUpTutor() {
        // reset error message
        priceEditText.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorHint));

        String priceStr = priceEditText.getText().toString();
        if (!priceStr.isEmpty()) {
            // create tutor object and send tutor sign up request
            int price = Integer.parseInt(priceStr);
            Tutor tutor = new Tutor(person.email, person.firstName, person.lastName, person.age,
                    person.profilePictureDirectory, person.location, price);

            sendTutorSignUpRequest(tutor);
        }
        else {
            // set error message for missing information
            priceEditText.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.errorFontColor));
            errorMessageTextView.setText(getString(R.string.error_missing_information));
        }
    }

    /**
     * Callback function for successful tutor sign up failure
     *
     * @param tutor tutor object
     */
    private void tutorSignUpSuccessfulCallback(Tutor tutor) {
        // starts add course activity
        Intent addCourseActivityIntent = new Intent(this,
                AddCourseActivity.class);
        addCourseActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.TUTOR);
        addCourseActivityIntent.putExtra("tutor", tutor);
        startActivity(addCourseActivityIntent);
        finish();
    }

    /**
     * Callback function for tutor sign up request failure
     *
     * @param statusCode reponse status code
     */
    private void tutorSignUpFailureCallback(int statusCode) {
        // display the appropriate error message based on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_tutor_exists));
        else
            errorMessageTextView.setText(getString(R.string.error_tutor_failed));
    }

    /**
     * Send tutor sign up request
     *
     * @param tutor tutor object
     */
    private void sendTutorSignUpRequest(final Tutor tutor) {
        // serialize tutor object to request param object
        RequestParams tutorInformationRequestParam = TutorSerializer.serialize(tutor);

        TutorMeRestClient.post(Urls.CREATE_TUTOR, tutorInformationRequestParam, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", new String(response)));
                tutorSignUpSuccessfulCallback(tutor);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", new String(errorResponse)));
                tutorSignUpFailureCallback(statusCode);
            }
        });
    }
}
