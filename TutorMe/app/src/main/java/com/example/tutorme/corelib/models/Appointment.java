package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Appointment class is a model for the appointment JSON object
 *
 *  * @author  Sai Janjanam
 *  * @version 1.0
 *  * @since   2019-04-20
 */
public class Appointment implements Serializable {
    public Student student;
    public Tutor tutor;
    public Course course;
    public String date;
    public String time;
    public int duration;
    public int price;
    public String location;
    public boolean accepted;
    public boolean declined;

    public Appointment(Student student, Course course, String date, String time, int duration,
                       int price, String location, boolean accepted, boolean declined) {
        this.student = student;
        this.tutor = course.tutor;
        this.course = course;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.price = price;
        this.location = location;
        this.accepted = accepted;
        this.declined = declined;
    }
}
