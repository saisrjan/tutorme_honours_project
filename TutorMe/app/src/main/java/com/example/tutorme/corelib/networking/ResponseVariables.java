package com.example.tutorme.corelib.networking;

/**
 * ResponseVariables class contains all variables associated with responses
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class ResponseVariables {
    public static final int OK_STATUS = 200;
    public static final int UPDATED_STATUS = 201;
    public static final int DELETED_STATUS = 204;
    public static final int BAD_REQUEST_STATUS = 400;
    public static final int NOT_FOUND_STATUS = 404;
    public static final int CONFLICT_STATUS = 409;
}