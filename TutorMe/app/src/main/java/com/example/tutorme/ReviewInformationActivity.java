package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.models.Review;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.ReviewSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

/**
 * ReviewInformationActivity class allows the user to write, view or delete review
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class ReviewInformationActivity extends AppCompatActivity {
    public static final String ERROR_TAG = "ReviewInformation";

    private TextView reviewInformationTitleText;
    private EditText studentNameEditText;
    private EditText courseCodeEditText;
    private EditText courseNameEditText;
    private EditText reviewRatingEditText;
    private EditText reviewTextEditText;
    private TextView errorTextView;

    private Person reviewer;
    private Person reviewee;
    private Course course;
    private Review review;
    private boolean writeReviewMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_information);

        // inflate all activity components
        reviewInformationTitleText = findViewById(R.id.text_review_information_title);
        studentNameEditText = findViewById(R.id.text_input_review_information_student_name);
        courseCodeEditText = findViewById(R.id.text_input_review_information_course_code);
        courseNameEditText = findViewById(R.id.text_input_review_information_course_name);
        reviewRatingEditText = findViewById(R.id.text_input_review_information_rating);
        reviewTextEditText = findViewById(R.id.text_input_review_information_review_text);
        errorTextView = findViewById(R.id.text_review_information_error);

        // disable student name, course code and course name fields
        studentNameEditText.setEnabled(false);
        courseCodeEditText.setEnabled(false);
        courseNameEditText.setEnabled(false);

        Button createReviewButton = findViewById(R.id.button_create_review);
        createReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearErrorMessage();
                getReviewInformation();
                sendCreateReviewRequest(review);
            }
        });

        Button deleteReviewButton = findViewById(R.id.button_delete_review);
        deleteReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDeleteReviewRequest(review);
            }
        });

        Intent intent = getIntent();

        // get write review mode from intent
        writeReviewMode = intent.getBooleanExtra(IntentKeys.WRITE_REVIEW_MODE, false);
        reviewer = (Person) intent.getSerializableExtra(IntentKeys.PERSON_REVIEWER_KEY);

        if (writeReviewMode) {
            // if the user is writing the intent get reviewer, reviewee, course from intent
            reviewee = (Person) intent.getSerializableExtra(IntentKeys.PERSON_REVIEWEE_KEY);
            course = (Course) intent.getSerializableExtra(IntentKeys.COURSE_KEY);

            // make appropriate review options available
            createReviewButton.setVisibility(View.VISIBLE);
            deleteReviewButton.setVisibility(View.INVISIBLE);
        }
        else {
            review = (Review) intent.getSerializableExtra(IntentKeys.REVIEW_KEYS);
            reviewRatingEditText.setEnabled(false);
            reviewTextEditText.setEnabled(false);

            // make appropriate review options available
            createReviewButton.setVisibility(View.INVISIBLE);
            if (review.reviewer.email.equals(reviewer.email))
                deleteReviewButton.setVisibility(View.VISIBLE);
            else
                deleteReviewButton.setVisibility(View.VISIBLE);
        }

        setReviewInformation();
    }

    /**
     * Set review information to fields in the activity
     */
    private void setReviewInformation() {
        String tutorName;
        String studentName;
        if (writeReviewMode) {
            // if write review mode is true, get information from different objects
            tutorName = getString(R.string.review_information_title_2) + " " + reviewee.firstName +
                    " " + reviewee.lastName;
            reviewInformationTitleText.setText(tutorName);
            studentName = reviewer.firstName + " " + reviewer.lastName;
            studentNameEditText.setText(studentName);
            courseCodeEditText.setText(course.code);
            courseNameEditText.setText(course.name);
        }
        else {
            // get information from review object
            studentName = review.reviewer.firstName + " " + review.reviewer.lastName;
            studentNameEditText.setText(studentName);
            courseCodeEditText.setText(review.course.code);
            courseNameEditText.setText(review.course.name);
            reviewRatingEditText.setText(String.format("Rating: %s", review.rating));
            reviewTextEditText.setText(review.reviewText);
        }
    }

    /**
     * Get review information from fields in the activity
     */
    private void getReviewInformation() {
        String ratingStr = reviewRatingEditText.getText().toString();
        String reviewText = reviewTextEditText.getText().toString();

        if (ratingStr.isEmpty() || reviewText.isEmpty())
            // make sure the necessary fields are filled, otherwise show the appropriate error message
            showMissingInformationError();
        else
            // create review object
            review = new Review(reviewer, reviewee, course, Integer.parseInt(ratingStr), reviewText);
    }

    /**
     * Clear existing error messages
     */
    private void clearErrorMessage() {
        errorTextView.setText("");
    }

    /**
     * Show missing information error message
     */
    private void showMissingInformationError() {
        errorTextView.setText(R.string.error_missing_review_information);
    }

    /**
     * Callback function for successful review requests
     */
    private void reviewRequestSuccessfulCallback() {
        finish();
    }

    /**
     * Callback function for create review request failure
     *
     * @param statusCode response status code
     */
    private void createReviewRequestFailureCallback(int statusCode) {
        // display the appropriate error message depending on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_create_review_exists);
        else
            errorTextView.setText(R.string.error_create_review_failed);
    }

    /**
     * Callback function for delete review request failure
     *
     * @param statusCode response status code
     */
    private void deleteReviewRequestFailureCallback(int statusCode) {
        // display the appropriate error message depending on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_delete_review_does_not_exist);
        else
            errorTextView.setText(R.string.error_delete_review_failed);
    }

    /**
     * Send create review request
     *
     * @param review review object
     */
    private void sendCreateReviewRequest(final Review review) {
        // serialize review object to request params object
        RequestParams reviewInformationRequestParams = ReviewSerializer.serialize(review);

        TutorMeRestClient.post(Urls.CREATE_REVIEW, reviewInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                reviewRequestSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                createReviewRequestFailureCallback(statusCode);
            }
        });
    }

    /**
     * Send delete review request
     *
     * @param review review object
     */
    private void sendDeleteReviewRequest(final Review review) {
        // serialize review object to request params object
        RequestParams reviewInformationRequestParams = ReviewSerializer.serialize(review);

        TutorMeRestClient.post(Urls.DELETE_REVIEW, reviewInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                reviewRequestSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                deleteReviewRequestFailureCallback(statusCode);
            }
        });
    }
}
