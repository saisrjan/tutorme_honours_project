package com.example.tutorme.corelib.networking;

import com.loopj.android.http.*;

/**
 * TutorMeRestClient class handles making requests to the server
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class TutorMeRestClient {
    private static AsyncHttpClient client = new AsyncHttpClient();

    /**
     * Sends a GET request
     *
     * @param url             url string
     * @param params          request parameters
     * @param responseHandler response handler object
     */
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(Urls.getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     * Sends POST request
     *
     * @param url             url string
     * @param params          request parameters
     * @param responseHandler response handler object
     */
    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(Urls.getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     * Sends DELETE request
     *
     * @param url             url string
     * @param params          request parameters
     * @param responseHandler response handler object
     */
    public static void delete(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.delete(Urls.getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     * Return client
     *
     * @return asynchttp client object
     */
    public static AsyncHttpClient getClient() {
        return client;
    }
}
