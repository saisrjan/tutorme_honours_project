package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Tutor;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * TutorSerializer class handler JSON deserialization and Tutor
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class TutorSerializer {
    /**
     * Converts a tutor object to a request param object containing the tutor information
     * @param tutor tutor object to convert
     * @return request param object containing tutor information
     */
    public static RequestParams serialize(Tutor tutor) {
        RequestParams personInformation = new RequestParams();
        personInformation.put(SerializerKeys.EMAIL_KEY, tutor.email);
        personInformation.put(SerializerKeys.DATE_JOINED, tutor.dateJoined);
        personInformation.put(SerializerKeys.OVERALL_RATING_OUT_OF_FIVE, tutor.overallRating);
        personInformation.put(SerializerKeys.PRICE_KEY, tutor.price);

        return personInformation;
    }

    /**
     * Converts a JSON object containing tutor information to a Tutor object
     * @param tutorJSONObject JSON object containing tutor information
     * @return Tutor object containing tutor information
     */
    public static Tutor deserialize(JSONObject tutorJSONObject) {
        Tutor tutor = null;

        try {
            String email = tutorJSONObject.getString(SerializerKeys.EMAIL_KEY);
            String firstName = tutorJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.FIRST_NAME_KEY);
            String lastName = tutorJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.LAST_NAME_KEY);
            int age = tutorJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getInt(SerializerKeys.AGE_KEY);
            String profilePictureDirectory = tutorJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.PROFILE_PIC_DIRECTORY);
            String location = tutorJSONObject.getJSONObject(SerializerKeys.PERSON_KEY).getString(SerializerKeys.LOCATION_KEY);
            String dateJoined = tutorJSONObject.getString(SerializerKeys.DATE_JOINED);
            int overallRating = tutorJSONObject.getInt(SerializerKeys.OVERALL_RATING_OUT_OF_FIVE);
            int price = tutorJSONObject.getInt(SerializerKeys.PRICE_KEY);

            tutor = new Tutor(email, firstName, lastName, age, profilePictureDirectory, location, dateJoined,
                    overallRating, price);
        } catch (JSONException e) {
            Log.e("Tutor", e.getMessage());
        }

        return tutor;
    }
}
