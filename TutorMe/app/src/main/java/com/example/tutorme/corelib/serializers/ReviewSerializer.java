package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.models.Review;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * ReviewSerializer class handler JSON deserialization and Review
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class ReviewSerializer {
    /**
     * Converts a review object to a request param object containing the review information
     * @param review review object to convert
     * @return request param object containing review information
     */
    public static RequestParams serialize(Review review) {
        RequestParams reviewRequestParams = new RequestParams();
        reviewRequestParams.put(SerializerKeys.REVIEWER_EMAIL_KEY, review.reviewer.email);
        reviewRequestParams.put(SerializerKeys.REVIEWEE_EMAIL_KEY, review.reviewee.email);
        reviewRequestParams.put(SerializerKeys.COURSE_CODE_KEY, review.course.code);
        reviewRequestParams.put(SerializerKeys.RATING_OUT_OF_FIVE_KEY, review.rating);
        reviewRequestParams.put(SerializerKeys.REVIEW_TEXT_KEY, review.reviewText);

        return reviewRequestParams;
    }

    /**
     * Converts a JSON object containing review information to a Review object
     * @param reviewJSONObject JSON object containing review information
     * @return Review object containing review information
     */
    public static Review deserialize(JSONObject reviewJSONObject) {
        Review review = null;

        try {
            JSONObject reviewerJSONObject = reviewJSONObject.getJSONObject(SerializerKeys.REVIEWER_PERSON_KEY);
            JSONObject revieweeJSONObject = reviewJSONObject.getJSONObject(SerializerKeys.REVIEWEE_PERSON_KEY);
            JSONObject courseJSONObject = reviewJSONObject.getJSONObject(SerializerKeys.COURSE_KEY);

            Person reviewer = PersonSerializer.deserializer(reviewerJSONObject);
            Person reviewee = PersonSerializer.deserializer(revieweeJSONObject);
            Course course = CourseSerializer.deserialize(courseJSONObject);

            int rating = reviewJSONObject.getInt(SerializerKeys.RATING_OUT_OF_FIVE_KEY);
            String reviewText = reviewJSONObject.getString(SerializerKeys.REVIEW_TEXT_KEY);

            review = new Review(reviewer, reviewee, course, rating, reviewText);
        } catch (JSONException e) {
            Log.e("CourseSerializer", e.getMessage());
        }

        return review;
    }
}
