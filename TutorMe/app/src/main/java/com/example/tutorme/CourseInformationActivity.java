package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.CourseSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.Arrays;

import cz.msebera.android.httpclient.Header;

/**
 * CourseInformationActivity class allows the user to create, view or edit the appointment
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class CourseInformationActivity extends AppCompatActivity {
    private static final String ERROR_TAG = "CourseInformation";

    private EditText courseCodeEditText;
    private EditText courseNameEditText;
    private EditText courseDescriptionEditText;
    private Spinner schoolSpinner;
    private Spinner gradeSpinner;
    private TextView errorMessageTextView;

    private Student student;
    private Tutor tutor;
    private Course course;
    private boolean addCourseMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_information);

        // inflate activity components
        courseCodeEditText = findViewById(R.id.text_input_course_information_code);
        courseNameEditText = findViewById(R.id.text_input_course_information_name);
        courseDescriptionEditText = findViewById(R.id.text_input_course_information_description);
        schoolSpinner = findViewById(R.id.spinner_course_information_school);
        gradeSpinner = findViewById(R.id.spinner_course_information_grade);
        errorMessageTextView = findViewById(R.id.text_course_information_error);

        Button addCourseButton = findViewById(R.id.button_course_information_add_course);
        addCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCourse();
            }
        });

        Button updateCourseButton = findViewById(R.id.button_course_information_update_course);
        updateCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCourse();
            }
        });

        Button deleteCourseButton = findViewById(R.id.button_course_information_delete_course);
        deleteCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCourse();
            }
        });

        Button bookAppointmentButton = findViewById(R.id.button_course_information_book_appointment);
        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookAppointment();
            }
        });

        Intent intent = getIntent();
        byte userType = intent.getByteExtra(UserType.USER_TYPE_KEY, (byte) 0);
        addCourseMode = intent.getBooleanExtra(IntentKeys.ADD_COURSE_MODE, false);

        if (userType == UserType.STUDENT) {
            // disable input when user is of type student
            courseCodeEditText.setEnabled(false);
            courseNameEditText.setEnabled(false);
            courseDescriptionEditText.setEnabled(false);
            schoolSpinner.setEnabled(false);
            gradeSpinner.setEnabled(false);

            // disable course editing options if the user is of type student
            addCourseButton.setVisibility(View.INVISIBLE);
            updateCourseButton.setVisibility(View.INVISIBLE);
            deleteCourseButton.setVisibility(View.INVISIBLE);
            bookAppointmentButton.setVisibility(View.VISIBLE);

            student = (Student) intent.getSerializableExtra(IntentKeys.STUDENT_KEY);
        }
        else {
            // disable book appointment option if user is of type tutor
            bookAppointmentButton.setVisibility(View.INVISIBLE);

            if (addCourseMode) {
                // if user is adding a new course
                tutor = (Tutor) intent.getSerializableExtra(IntentKeys.TUTOR_KEY);

                addCourseButton.setVisibility(View.VISIBLE);
                updateCourseButton.setVisibility(View.INVISIBLE);
                deleteCourseButton.setVisibility(View.INVISIBLE);
            }
            else {
                // if tutor is viewing course information
                // prevent them from changing course code
                courseCodeEditText.setEnabled(false);

                addCourseButton.setVisibility(View.INVISIBLE);
                updateCourseButton.setVisibility(View.VISIBLE);
                deleteCourseButton.setVisibility(View.VISIBLE);
            }
        }

        if (!addCourseMode) {
            // if user is viewing course information
            // set course information
            course = (Course) intent.getSerializableExtra(IntentKeys.COURSE_KEY);

            // set course information
            setCourseInformation(course);
        }
    }

    /**
     * Set course information in activity fields
     * @param course course object
     */
    private void setCourseInformation(final Course course) {
        courseCodeEditText.setText(course.code);
        courseNameEditText.setText(course.name);
        courseDescriptionEditText.setText(course.description);

        String[] schoolsArray = getResources().getStringArray(R.array.list_of_universities);
        schoolSpinner.setSelection(Arrays.asList(schoolsArray).indexOf(course.school));

        String[] gradesArray = getResources().getStringArray(R.array.list_of_grades);
        gradeSpinner.setSelection(Arrays.asList(gradesArray).indexOf(course.grade));
    }

    /**
     * Get course information from activity fields
     */
    private void getCourseInformation() {
        String code = courseCodeEditText.getText().toString();
        String name = courseNameEditText.getText().toString();
        String description = courseDescriptionEditText.getText().toString();
        String school = schoolSpinner.getSelectedItem().toString();
        String grade = gradeSpinner.getSelectedItem().toString();

        if (addCourseMode) {
            // create new course object if user is creating a new course
            course = new Course(tutor, code, name, description, school, grade);
        }
        else {
            // update existing course object
            course.code = courseCodeEditText.getText().toString();
            course.name = courseNameEditText.getText().toString();
            course.description = courseDescriptionEditText.getText().toString();
            course.school = schoolSpinner.getSelectedItem().toString();
            course.grade = gradeSpinner.getSelectedItem().toString();
        }
    }

    /**
     * Add course function
     */
    private void addCourse() {
        // clear existing error messages
        clearErrorMessage();

        // get course information from activity fields
        getCourseInformation();

        if (course.code.isEmpty() || course.name.isEmpty() || course.description.isEmpty() ||
                course.school.isEmpty() || course.grade.isEmpty())
            // check if any of the course information is blank
            // if so, set the appropriate error message
            errorMessageTextView.setText(getString(R.string.error_course_missing_information));
        else
            // call add course request
            sendAddCourseRequest(course);
    }

    /**
     * Update course function
     */
    private void updateCourse() {
        // clear existing error messages
        clearErrorMessage();

        // get course information from activity fields
        getCourseInformation();

        if (course.code.isEmpty() || course.name.isEmpty() || course.description.isEmpty() ||
                course.school.isEmpty() || course.grade.isEmpty())
            // check if any of the course information is blank
            // if so, set the appropriate error message
            errorMessageTextView.setText(getString(R.string.error_course_missing_information));
        else
            // call update course request
            sendUpdateCourseRequest(course);
    }

    /**
     * Delete course function
     */
    private void deleteCourse() {
        // clear existing error messages
        clearErrorMessage();

        // get course information from activity fields
        getCourseInformation();

        // call delete course request
        sendDeleteCourseRequest(course);
    }

    /**
     * Starts the Appointment Information Activity
     */
    private void bookAppointment() {
        Intent appointmentInformationActivityIntent = new Intent(this, AppointmentInformationActivity.class);
        appointmentInformationActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        appointmentInformationActivityIntent.putExtra(IntentKeys.BOOK_APPOINTMENT_MODE, true);
        appointmentInformationActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);
        appointmentInformationActivityIntent.putExtra(IntentKeys.TUTOR_KEY, tutor);
        appointmentInformationActivityIntent.putExtra(IntentKeys.COURSE_KEY, course);
        startActivity(appointmentInformationActivityIntent);
    }

    /**
     * Clear existing error message
     */
    private void clearErrorMessage() {
        errorMessageTextView.setText("");
    }

    /**
     * Callback function for course request successful responses
     */
    private void courseSuccessfulCallback() {
        // closes this activity
        finish();
    }

    /**
     * Callback function for add course request failure response
     * @param statusCode status code
     */
    private void addCourseFailureCallback(int statusCode) {
        // set the appropriate error message based on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_add_course_exists));
        else
            errorMessageTextView.setText(getString(R.string.error_add_course_failed));
    }

    /**
     * Callback function for update course request failure response
     * @param statusCode status code
     */
    private void updateCourseFailureCallback(int statusCode) {
        // set the appropriate error message based on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_update_course_not_exist));
        else
            errorMessageTextView.setText(getString(R.string.error_update_course_failed));
    }

    /**
     * Callback function for delete course request failure response
     * @param statusCode status code
     */
    private void deleteCourseFailureCallback(int statusCode) {
        // set the appropriate error message based on the status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_delete_course_not_exist));
        else
            errorMessageTextView.setText(getString(R.string.error_delete_course_failed));
    }

    /**
     * Sends add course request
     *
     * @param course course object
     */
    private void sendAddCourseRequest(final Course course) {
        // serializes course object into request params object
        RequestParams courseInformationRequestParams = CourseSerializer.serialize(course);

        TutorMeRestClient.post(Urls.ADD_COURSE, courseInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                courseSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                addCourseFailureCallback(statusCode);
            }
        });
    }

    /**
     * Sends update course request
     *
     * @param course course object
     */
    private void sendUpdateCourseRequest(final Course course) {
        // serializes course object into request params object
        RequestParams courseInformationRequestParams = CourseSerializer.serialize(course);

        TutorMeRestClient.post(Urls.UPDATE_COURSE, courseInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                courseSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                updateCourseFailureCallback(statusCode);
            }
        });
    }

    /**
     * Sends delete course request
     *
     * @param course course object
     */
    private void sendDeleteCourseRequest(final Course course) {
        // serializes course object into request params object
        RequestParams courseInformationRequestParams = CourseSerializer.serialize(course);

        TutorMeRestClient.post(Urls.DELETE_COURSE, courseInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                courseSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                deleteCourseFailureCallback(statusCode);
            }
        });
    }
}