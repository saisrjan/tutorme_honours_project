package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Course class is a model for the course JSON object
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Course implements Serializable {
    public Tutor tutor;
    public String code;
    public String name;
    public String description;
    public String school;
    public String grade;

    public Course(Tutor tutor, String code, String name, String description,
                  String school, String grade) {
        this.tutor = tutor;
        this.code = code;
        this.name = name;
        this.description = description;
        this.school = school;
        this.grade = grade;
    }
}
