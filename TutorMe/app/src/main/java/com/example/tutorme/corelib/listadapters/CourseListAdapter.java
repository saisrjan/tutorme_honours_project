package com.example.tutorme.corelib.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tutorme.CourseListAdapterHandler;
import com.example.tutorme.R;
import com.example.tutorme.corelib.models.Course;

import java.util.List;

/**
 * CourseListAdapter class handles view creation and data set changes
 * for the Recycler View
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class CourseListAdapter extends RecyclerView.Adapter<CourseListAdapter.CourseViewHolder> {
    private List<Course> courseList;
    private CourseListAdapterHandler listAdapterHandler;

    public class CourseViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public CourseViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public CourseListAdapter(List<Course> courseList, CourseListAdapterHandler listAdapterHandler) {
        this.courseList = courseList;
        this.listAdapterHandler = listAdapterHandler;
    }

    @Override
    public CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.course_list_item,
                parent, false);

        viewItem.setClickable(true);
        return new CourseViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(CourseViewHolder courseViewHolder, int position) {
        final Course course = courseList.get(position);

        // inflate all components of the course list item
        TextView courseCodeTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_course_code);
        TextView courseNameTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_course_name);
        TextView schoolTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_school);

        // set values to the appropriate views
        courseCodeTextView.setText(course.code);
        courseNameTextView.setText(course.name);
        schoolTextView.setText(course.school);

        courseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // callback function to start course information activity
                listAdapterHandler.startCourseInformationActivity(course);
            }
        });
    }

    public int getItemCount() {
        return courseList.size();
    }

    /**
     * Update the course data set and notify the list view of the data change
     *
     * @param courseList list of course objects
     */
    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
        notifyDataSetChanged();
    }
}
