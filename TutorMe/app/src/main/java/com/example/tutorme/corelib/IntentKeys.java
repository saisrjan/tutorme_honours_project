package com.example.tutorme.corelib;

/**
 * IntentKeys class for keys used in intents
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class IntentKeys {
    public static final String ADD_COURSE_MODE = "add_course_mode";
    public static final String BOOK_APPOINTMENT_MODE = "book_appointment_mode";
    public static final String WRITE_REVIEW_MODE = "write_review_mode";

    public static final String PERSON_KEY = "person";
    public static final String COURSE_KEY = "course";
    public static final String TUTOR_KEY = "tutor";
    public static final String STUDENT_KEY = "student";
    public static final String APPOINTMENT_KEYS = "appointment";
    public static final String REVIEW_KEYS = "review";
    public static final String PERSON_REVIEWER_KEY = "person_reviewer";
    public static final String PERSON_REVIEWEE_KEY = "person_reviewee";
}
