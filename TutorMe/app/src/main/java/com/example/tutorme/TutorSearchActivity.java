package com.example.tutorme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.listadapters.TutorListAdapter;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.StudentSerializer;
import com.example.tutorme.corelib.serializers.TutorSerializer;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class TutorSearchActivity extends AppCompatActivity {
    public static String ERROR_TAG = "TutorSearchActivity";

    private EditText tutorSearchEditText;
    private TutorListAdapter adapter;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        student = (Student) intent.getSerializableExtra(IntentKeys.STUDENT_KEY);

        // inflate all activity components
        tutorSearchEditText = findViewById(R.id.text_input_search_tutor);

        Button searchButton = findViewById(R.id.button_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchQuery();
            }
        });

        // inflate and initiate the course list and its adapter
        RecyclerView tutorListRecyclerView = findViewById(R.id.list_view_tutor_search_results);
        adapter = new TutorListAdapter(new ArrayList<Tutor>(), this);

        tutorListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        tutorListRecyclerView.setAdapter(adapter);
        tutorListRecyclerView.setHasFixedSize(true);
    }

    /**
     * Search query function
     */
    private void searchQuery() {
        // get search query from edit text field
        String query = tutorSearchEditText.getText().toString();
        if (!query.isEmpty())
            sendTutorSearchRequest(query);
    }

    /**
     * Update tutor list. Callback function for tutor search request
     *
     * @param tutorList list of tutors
     */
    private void updateTutorList(List<Tutor> tutorList) {
        adapter.setTutorList(tutorList);
    }

    /**
     * Send tutor search request
     *
     * @param query query string
     */
    private void sendTutorSearchRequest(String query) {
        RequestParams searchQueryRequestInformation = StudentSerializer.serialize(student);
        searchQueryRequestInformation.put("query", query);

        TutorMeRestClient.get(Urls.SEARCH_TUTORS, searchQueryRequestInformation, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i(ERROR_TAG, String.format("Received: %s", response));

                List<Tutor> tutorList = new ArrayList<>();
                try {
                    JSONArray tutorJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < tutorJSONArray.length(); i++) {
                        JSONObject tutorJSONObject = tutorJSONArray.getJSONObject(i);
                        tutorList.add(TutorSerializer.deserialize(tutorJSONObject));
                    }
                } catch(JSONException e) {
                    Log.e(ERROR_TAG, e.getMessage());
                    return;
                }
                updateTutorList(tutorList);
            }
        });
    }

    /**
     * Start tutor information activity
     *
     * @param tutor tutor object
     */
    public void startTutorInformationActivity(Tutor tutor) {
        Intent tutorInformationActivityIntent = new Intent(this, TutorInformationActivity.class);
        tutorInformationActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        tutorInformationActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);
        tutorInformationActivityIntent.putExtra(IntentKeys.TUTOR_KEY, tutor);
        startActivity(tutorInformationActivityIntent);
    }
}
