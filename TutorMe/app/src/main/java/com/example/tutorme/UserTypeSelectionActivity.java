package com.example.tutorme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.models.Person;

public class UserTypeSelectionActivity extends AppCompatActivity {

    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type_selection);

        Button studentSelectButton = findViewById(R.id.button_student_select);
        Button tutorSelectButton = findViewById(R.id.button_tutor_select);

        studentSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStudentSignUpActivity();
            }
        });

        tutorSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTutorSignUpActivity();
            }
        });

        Intent intent = getIntent();
        person = (Person) intent.getSerializableExtra(IntentKeys.PERSON_KEY);
    }

    /**
     * Start student sign up activity
     */
    private void showStudentSignUpActivity() {
        // initiate student sign up activity for student
        Intent studentSignUpIntent = new Intent(this, StudentSignUpActivity.class);
        studentSignUpIntent.putExtra(IntentKeys.PERSON_KEY, person);
        startActivity(studentSignUpIntent);
    }

    /**
     * Start tutor sign up activity
     */
    private void showTutorSignUpActivity() {
        // initiate tutor sign up activity for student
        Intent tutorSignUpIntent = new Intent(this, TutorSignUpActivity.class);
        tutorSignUpIntent.putExtra(IntentKeys.PERSON_KEY, person);
        startActivity(tutorSignUpIntent);
    }
}
