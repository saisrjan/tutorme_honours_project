package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Appointment;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * AppointmentSerializer class handler JSON deserialization and Appointment
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class AppointmentSerializer {
    /**
     * Converts a appointment object to a request param object containing the appointment information
     * @param appointment appointment object to convert
     * @return request param object containing appointment information
     */
    public static RequestParams serialize(Appointment appointment) {
        RequestParams appointmentRequestParams = new RequestParams();
        appointmentRequestParams.put(SerializerKeys.STUDENT_EMAIL_KEY, appointment.student.email);
        appointmentRequestParams.put(SerializerKeys.TUTOR_EMAIL_KEY, appointment.tutor.email);
        appointmentRequestParams.put(SerializerKeys.COURSE_CODE_KEY, appointment.course.code);
        appointmentRequestParams.put(SerializerKeys.DATE_KEY, appointment.date);
        appointmentRequestParams.put(SerializerKeys.TIME_KEY, appointment.time);
        appointmentRequestParams.put(SerializerKeys.DURATION_KEY, appointment.duration);
        appointmentRequestParams.put(SerializerKeys.PRICE_KEY, appointment.price);
        appointmentRequestParams.put(SerializerKeys.LOCATION_KEY, appointment.location);
        appointmentRequestParams.put(SerializerKeys.ACCEPTED_KEY, appointment.accepted);
        appointmentRequestParams.put(SerializerKeys.DECLINED_KEY, appointment.declined);

        return appointmentRequestParams;
    }

    /**
     * Converts a JSON object containing appointment information to a Appointment object
     * @param appointmentJSONObject JSON object containing appointment information
     * @return Appointment object containing appointment information
     */
    public static Appointment deserialize(JSONObject appointmentJSONObject) {
        Appointment appointment = null;

        try {
            JSONObject studentJSONObject = appointmentJSONObject.getJSONObject(SerializerKeys.STUDENT_KEY);
            JSONObject tutorJSONObject = appointmentJSONObject.getJSONObject(SerializerKeys.TUTOR_KEY);
            JSONObject courseJSONObject = appointmentJSONObject.getJSONObject(SerializerKeys.COURSE_KEY);

            Student student = StudentSerializer.deserializer(studentJSONObject);
            Tutor tutor = TutorSerializer.deserialize(tutorJSONObject);
            Course course = CourseSerializer.deserialize(courseJSONObject);

            String date = appointmentJSONObject.getString(SerializerKeys.DATE_KEY);
            String time = appointmentJSONObject.getString(SerializerKeys.TIME_KEY);
            int duration = appointmentJSONObject.getInt(SerializerKeys.DURATION_KEY);
            int price = appointmentJSONObject.getInt(SerializerKeys.PRICE_KEY);
            String location = appointmentJSONObject.getString(SerializerKeys.LOCATION_KEY);
            boolean accepted = appointmentJSONObject.getBoolean(SerializerKeys.ACCEPTED_KEY);
            boolean declined = appointmentJSONObject.getBoolean(SerializerKeys.DECLINED_KEY);

            appointment = new Appointment(student, course, date, time, duration, price,
                    location, accepted, declined);
        } catch (JSONException e) {
            Log.e("AppointmentSerializer", e.getMessage());
        }

        return appointment;
    }
}