package com.example.tutorme.corelib.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tutorme.R;
import com.example.tutorme.TutorSearchActivity;
import com.example.tutorme.corelib.models.Tutor;

import java.util.List;

/**
 * TutorListAdapter class handles view creation and data set changes
 * for the Recycler View
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class TutorListAdapter extends RecyclerView.Adapter<TutorListAdapter.TutorViewHolder> {
    private List<Tutor> tutorList;
    private TutorSearchActivity tutorSearchActivity;

    public class TutorViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public TutorViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public TutorListAdapter(List<Tutor> tutorList, TutorSearchActivity tutorSearchActivity) {
        this.tutorList = tutorList;
        this.tutorSearchActivity = tutorSearchActivity;
    }

    @Override
    public TutorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.tutor_list_item,
                parent, false);

        viewItem.setClickable(true);
        return new TutorViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(TutorViewHolder tutorViewHolder, int position) {
        final Tutor tutor = tutorList.get(position);

        // inflate the all components of the tutor list item
        TextView tutorNameTextView = tutorViewHolder.itemView.findViewById(R.id.text_list_item_tutor_name);
        TextView ratingTextView = tutorViewHolder.itemView.findViewById(R.id.text_list_item_rating);
        TextView dateJoinedTextView = tutorViewHolder.itemView.findViewById(R.id.text_list_item_date_joined);

        // set the values to the appropriate views
        String name = tutor.firstName + " " + tutor.lastName;
        tutorNameTextView.setText(name);

        String rating = "Rating: " + tutor.overallRating;
        ratingTextView.setText(rating);

        String dateJoined = "Date Joined: " + tutor.dateJoined;
        dateJoinedTextView.setText(dateJoined);

        tutorViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // callback function to start tutor information activity
                tutorSearchActivity.startTutorInformationActivity(tutor);
            }
        });
    }

    public int getItemCount() {
        return tutorList.size();
    }

    /**
     * Update the tutor dataset and notify the list view of the data change
     *
     * @param tutorList list of tutor objects
     */
    public void setTutorList(List<Tutor> tutorList) {
        this.tutorList = tutorList;
        notifyDataSetChanged();
    }
}
