package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Review class is a model for the review JSON object
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Review implements Serializable {
    public Person reviewer;
    public Person reviewee;
    public Course course;
    public int rating;
    public String reviewText;

    public Review (Person reviewer, Person reviewee, Course course, int rating, String reviewText) {
        this.reviewer = reviewer;
        this.reviewee = reviewee;
        this.course = course;
        this.rating = rating;
        this.reviewText = reviewText;
    }
}
