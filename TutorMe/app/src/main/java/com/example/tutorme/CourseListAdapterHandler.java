package com.example.tutorme;

import com.example.tutorme.corelib.models.Course;

public interface CourseListAdapterHandler {
    void startCourseInformationActivity(final Course course);
}
