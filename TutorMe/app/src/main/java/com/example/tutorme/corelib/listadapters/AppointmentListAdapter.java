package com.example.tutorme.corelib.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tutorme.HomePageActivity;
import com.example.tutorme.R;
import com.example.tutorme.corelib.models.Appointment;

import java.util.List;

/**
 * AppointmentListAdapter class handles view creation and data set changes
 * for the Recycler View
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.AppointmentViewHolder> {
    private List<Appointment> appointmentList;
    private HomePageActivity homePageActivity;

    public class AppointmentViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public AppointmentViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public AppointmentListAdapter(List<Appointment> appointmentList, HomePageActivity homePageActivity) {
        this.appointmentList = appointmentList;
        this.homePageActivity = homePageActivity;
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_list_item,
                parent, false);

        viewItem.setClickable(true);
        return new AppointmentViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder appointmentViewHolder, int position) {
        final Appointment appointment = appointmentList.get(position);

        // inflate all components of appointment list item
        ImageView appointmentStatusImageView = appointmentViewHolder.itemView.findViewById(R.id.image_appointment_information_status);
        TextView courseCodeTextView = appointmentViewHolder.itemView.findViewById(R.id.text_list_item_appointment_course_code);
        TextView tutorNameTextView = appointmentViewHolder.itemView.findViewById(R.id.text_list_item_appointment_tutor_name);
        TextView dateAndTimeTextView = appointmentViewHolder.itemView.findViewById(R.id.text_list_item_appointment_date_and_time);
        TextView locationTextView = appointmentViewHolder.itemView.findViewById(R.id.text_list_item_appointment_location);

        if (appointment.accepted) {
            // green icon should be set if the appointment is accepted
            appointmentStatusImageView.setImageResource(R.drawable.iconfinder_bullet_green_44189);
        }
        else if (appointment.declined) {
            // red icon should be set if the appointment is declined
            appointmentStatusImageView.setImageResource(R.drawable.iconfinder_bullet_red_44191);
        }
        else {
            // yellow icon should be set if the appointment is undecided
            appointmentStatusImageView.setImageResource(R.drawable.iconfinder_bullet_yellow_44192);
        }

        // set appointment values to the appropriate views
        courseCodeTextView.setText(appointment.course.code);

        String tutorName = appointment.tutor.firstName + " " + appointment.tutor.lastName;
        tutorNameTextView.setText(tutorName);

        String dateAndTime = appointment.date + " " + appointment.time;
        dateAndTimeTextView.setText(dateAndTime);

        locationTextView.setText(appointment.location);

        appointmentViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // callback function to start appointment information activity
                homePageActivity.startAppointmentInformationActivity(appointment);
            }
        });
    }

    public int getItemCount() {
        return appointmentList.size();
    }

    /**
     * Update appointment list data set and notify list view of the data change
     *
     * @param appointmentList list of appointment objects
     */
    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
        notifyDataSetChanged();
    }
}

