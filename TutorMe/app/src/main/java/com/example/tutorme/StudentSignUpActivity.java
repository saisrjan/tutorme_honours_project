package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.StudentSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

/**
 * StudentSignUpActivity class allows the student to sign up
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class StudentSignUpActivity extends AppCompatActivity {
    public static final String ERROR_TAG = "StudentSignUpActivity";

    private Spinner schoolSpinner;
    private TextView errorMessageTextView;

    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_sign_up);

        // inflate activity components
        schoolSpinner = findViewById(R.id.spinner_student_school);
        errorMessageTextView = findViewById(R.id.text_student_sign_up_error);

        Button studentSignUpButton = findViewById(R.id.button_student_sign_up);
        studentSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUpStudent();
            }
        });

        Intent intent = getIntent();
        // get person object from intent
        person = (Person) intent.getSerializableExtra(IntentKeys.PERSON_KEY);
    }

    /**
     * Sign up student function
     */
    private void signUpStudent() {
        String school = schoolSpinner.getSelectedItem().toString();

        sendStudentSignUpRequest(new Student(person.email, person.firstName,
                person.lastName, person.age, person.profilePictureDirectory, person.location, school));
    }

    /**
     * Callback function for successful student sign up
     *
     * @param student student object
     */
    private void studentSignUpSuccessfulCallback(Student student) {
        Intent homePageActivityIntent = new Intent(this, HomePageActivity.class);
        homePageActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        homePageActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);
        startActivity(homePageActivityIntent);
    }

    /**
     * Callback function for student sign up failure
     *
     * @param statusCode response status code
     */
    private void studentSignUpFailureCallback(int statusCode) {
        // display appropriate error message based on status code
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_student_exists));
        else
            errorMessageTextView.setText(getString(R.string.error_student_failed));
    }

    /**
     * Send student sign up request
     *
     * @param student student object
     */
    public void sendStudentSignUpRequest(final Student student) {
        // serialize student object to request params
        RequestParams studentInformation = StudentSerializer.serialize(student);

        TutorMeRestClient.post(Urls.CREATE_STUDENT, studentInformation, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                studentSignUpSuccessfulCallback(student);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                studentSignUpFailureCallback(statusCode);
            }
        });
    }
}
