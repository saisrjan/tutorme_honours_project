package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Tutor class is a model for the tutor JSON object
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Tutor extends Person implements Serializable {
    public String dateJoined;
    public int overallRating;
    public int price;

    public Tutor(String email, String firstName, String lastName, int age, String profilePictureDirectory,
                 String location, int price) {
        super(email, firstName, lastName, age, profilePictureDirectory, location);

        this.dateJoined = "";
        this.overallRating = 0;
        this.price = price;
    }

    public Tutor(String email, String firstName, String lastName, int age, String profilePictureDirectory,
                   String location, String dateJoined, int overallRating, int price) {
        super(email, firstName, lastName, age, profilePictureDirectory, location);

        this.dateJoined = dateJoined;
        this.overallRating = overallRating;
        this.price = price;
    }
}
