package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.listadapters.CourseListAdapter;
import com.example.tutorme.corelib.models.Review;
import com.example.tutorme.corelib.listadapters.ReviewListAdapter;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.CourseSerializer;
import com.example.tutorme.corelib.serializers.ReviewSerializer;
import com.example.tutorme.corelib.serializers.TutorSerializer;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class TutorInformationActivity extends AppCompatActivity implements CourseListAdapterHandler {
    public static final String ERROR_TAG = "TutorInformation";

    private TextView tutorNameTitleEditText;
    private TextView tutorRatingEditText;
    private EditText tutorAgeEditText;
    private EditText tutorPriceEditText;
    private EditText tutorEmailEditText;
    private EditText tutorLocationEditText;

    private CourseListAdapter courseListAdapter;
    private ReviewListAdapter reviewListAdapter;

    private byte userType;
    private Student student;
    private Tutor tutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_information);

        // inflate all activity components
        tutorNameTitleEditText = findViewById(R.id.text_tutor_info_name_title);
        tutorRatingEditText = findViewById(R.id.text_tutor_info_rating);
        tutorAgeEditText = findViewById(R.id.text_input_tutor_info_age);
        tutorPriceEditText = findViewById(R.id.text_input_tutor_info_price);
        tutorEmailEditText = findViewById(R.id.text_input_tutor_info_email);
        tutorLocationEditText = findViewById(R.id.text_input_tutor_info_location);

        Intent intent = getIntent();
        // get tutor object from intent
        userType = intent.getByteExtra(UserType.USER_TYPE_KEY, (byte) 0);
        if (userType == UserType.STUDENT)
            student = (Student) intent.getSerializableExtra(IntentKeys.STUDENT_KEY);
        tutor = (Tutor) intent.getSerializableExtra(IntentKeys.TUTOR_KEY);

        RecyclerView courseListRecyclerView = findViewById(R.id.list_view_tutor_info_courses);
        courseListAdapter = new CourseListAdapter(new ArrayList<Course>(), this);

        courseListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        courseListRecyclerView.setAdapter(courseListAdapter);
        courseListRecyclerView.setHasFixedSize(true);

        RecyclerView reviewListRecyclerView = findViewById(R.id.list_view_tutor_info_reviews);
        reviewListAdapter = new ReviewListAdapter(new ArrayList<Review>(), this);

        reviewListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        reviewListRecyclerView.setAdapter(reviewListAdapter);
        reviewListRecyclerView.setHasFixedSize(true);

        setTutorInformation(tutor);

        sendGetCoursesRequest(tutor);
        sendGetReviewsRequest(tutor);
    }

    protected void onStart() {
        sendGetCoursesRequest(tutor);
        sendGetReviewsRequest(tutor);

        super.onStart();
    }

    /**
     * Set tutor information to activity fields
     *
     * @param tutor tutor object
     */
    private void setTutorInformation(Tutor tutor) {
        String tutorFullName = tutor.firstName + " " + tutor.lastName;
        tutorNameTitleEditText.setText(tutorFullName);

        String tutorRatingStr = "Rating: " + tutor.overallRating;
        tutorRatingEditText.setText(tutorRatingStr);

        String tutorAgeStr = "Age: " + tutor.age;
        tutorAgeEditText.setText(tutorAgeStr);

        tutorLocationEditText.setText(tutor.location);
        tutorEmailEditText.setText(tutor.email);

        String price = "$"+tutor.price;
        tutorPriceEditText.setText(price);
    }

    /**
     * Callback function to start course information activity
     *
     * @param course course object
     */
    public void startCourseInformationActivity(final Course course) {
        Intent courseInformationActivityIntent = new Intent(this, CourseInformationActivity.class);
        courseInformationActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        if (userType == UserType.STUDENT)
            courseInformationActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);
        courseInformationActivityIntent.putExtra(IntentKeys.TUTOR_KEY, tutor);
        courseInformationActivityIntent.putExtra(IntentKeys.COURSE_KEY, course);
        startActivity(courseInformationActivityIntent);
    }

    /**
     * Callback function to start reviews information activity
     *
     * @param review review object
     */
    public void startReviewInformationActivity(final Review review) {
        Intent courseInformationActivityIntent = new Intent(this, ReviewInformationActivity.class);
        courseInformationActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        courseInformationActivityIntent.putExtra("person_reviewer", student);
        courseInformationActivityIntent.putExtra("review", review);
        startActivity(courseInformationActivityIntent);
    }

    /**
     * Callback function for get courses request
     *
     * @param courseList list of courses
     */
    private void updateCourseList(List<Course> courseList) {
        courseListAdapter.setCourseList(courseList);
    }

    /**
     * Callback function for get reviews request
     *
     * @param reviewList list of reviews
     */
    private void updateReviewList(List<Review> reviewList) {
        reviewListAdapter.setReviewList(reviewList);
    }

    /**
     * Send get courses request
     *
     * @param tutor tutor object
     */
    private void sendGetCoursesRequest(final Tutor tutor) {
        // serialize the tutor object into request parameters
        // attributes are turned into key and value pairs
        RequestParams tutorInformationRequestParams = TutorSerializer.serialize(tutor);

        TutorMeRestClient.get(Urls.GET_COURSES, tutorInformationRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("TutorInformation", String.format("Received: %s", response));

                // parse JSON list into course objects
                List<Course> courseList = new ArrayList<>();
                try {
                    JSONArray courseJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < courseJSONArray.length(); i++) {
                        JSONObject courseJSONObject = courseJSONArray.getJSONObject(i);
                        courseList.add(CourseSerializer.deserialize(courseJSONObject));
                    }
                } catch (JSONException e) {
                    Log.e(ERROR_TAG,
                            String.format("Incorrect JSON response. Received: %s", response));
                }

                // update course list with new data set
                updateCourseList(courseList);
            }
        });
    }

    /**
     * Send get review request
     *
     * @param tutor tutor object
     */
    private void sendGetReviewsRequest(final Tutor tutor) {
        // serialize the tutor object into request parameters
        // attributes are turned into key and value pairs
        RequestParams tutorInformationRequestParams = TutorSerializer.serialize(tutor);

        TutorMeRestClient.get(Urls.GET_REVIEWS, tutorInformationRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i("TutorInformation", String.format("Received: %s", response));

                // parse JSON list into review objects
                List<Review> reviewList = new ArrayList<>();
                try {
                    JSONArray reviewJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < reviewJSONArray.length(); i++) {
                        JSONObject reviewJSONObject = reviewJSONArray.getJSONObject(i);
                        reviewList.add(ReviewSerializer.deserialize(reviewJSONObject));
                    }
                } catch (JSONException e) {
                    Log.e(ERROR_TAG,
                            String.format("Incorrect JSON response. Received: %s", response));
                }

                // update review list with new data set
                updateReviewList(reviewList);
            }
        });
    }
}
