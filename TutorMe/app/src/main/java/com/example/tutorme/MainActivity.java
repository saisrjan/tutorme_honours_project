package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.loopj.android.http.PersistentCookieStore;

/**
 * MainActivity class starting point of app
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class MainActivity extends AppCompatActivity {
    public static PersistentCookieStore myCookieStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button studentLoginButton = findViewById(R.id.button_student_login);
        Button tutorLoginButton = findViewById(R.id.button_tutor_login);
        Button signUpButton = findViewById(R.id.button_sign_up);

        // show student login activity
        studentLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStudentLogin();
            }
        });

        // show tutor login activity
        tutorLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTutorLogin();
            }
        });

        // show sign up activity
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSignUp();
            }
        });

        myCookieStore = new PersistentCookieStore(getApplicationContext());
        TutorMeRestClient.getClient().setCookieStore(myCookieStore);
    }

    /**
     * Initiate student login activity
     */
    private void showStudentLogin() {
        // initiate login activity for student
        Intent studentLoginIntent = new Intent(this, LoginActivity.class);
        studentLoginIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
        startActivity(studentLoginIntent);
    }

    /**
     * Initiate tutor login activity
     */
    private void showTutorLogin() {
        // initiate login activity for tutor
        Intent tutorLoginIntent = new Intent(this, LoginActivity.class);
        tutorLoginIntent.putExtra(UserType.USER_TYPE_KEY, UserType.TUTOR);
        startActivity(tutorLoginIntent);
    }

    /**
     * Initiate sign sup activity
     */
    private void showSignUp() {
        // initiate sign up activity for student
        Intent studentSignUpIntent = new Intent(this, SignUpActivity.class);
        startActivity(studentSignUpIntent);
    }
}
