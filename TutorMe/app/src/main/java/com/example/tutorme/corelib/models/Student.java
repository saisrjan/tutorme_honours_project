package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Student class is a model for the student JSON object
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Student extends Person implements Serializable {
    public String currentSchool;
    public int overallRating;

    public Student(String email, String firstName, String lastName, int age, String profilePictureDirectory,
                   String location, String currentSchool) {
        super(email, firstName, lastName, age, profilePictureDirectory, location);

        this.currentSchool = currentSchool;
        this.overallRating = 0;
    }

    public Student(String email, String firstName, String lastName, int age, String profilePictureDirectory,
                   String location, String currentSchool, int overallRating) {
        super(email, firstName, lastName, age, profilePictureDirectory, location);

        this.currentSchool = currentSchool;
        this.overallRating = overallRating;
    }
}
