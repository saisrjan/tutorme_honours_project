package com.example.tutorme;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.PersonSerializer;
import com.example.tutorme.corelib.serializers.SerializerKeys;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.theartofdev.edmodo.cropper.CropImage;

import cz.msebera.android.httpclient.Header;

/**
 * SignUpActivity class allows the user sign up
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class SignUpActivity extends AppCompatActivity {
    public static final String ERROR_TAG = "SignUpActivity";

    // permission request result code
    private static final byte MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;

    // account information validation result codes
    private static final byte ACCOUNT_INFORMATION_VALIDATION_SUCCESS = 0;
    private static final byte ACCOUNT_INFORMATION_VALIDATION_MISSING_INFO = 1;
    private static final byte ACCOUNT_INFORMATION_VALIDATION_PASSWORD_MISMATCH = 2;
    private static final byte ACCOUNT_INFORMATION_VALIDATION_UNDERAGE = 3;

    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText ageEditText;
    private EditText locationEditText;
    private TextView errorMessageTextView;
    private ImageButton profilePictureImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // inflate sign up activity components
        emailEditText = findViewById(R.id.text_input_email);
        passwordEditText = findViewById(R.id.text_input_password);
        confirmPasswordEditText = findViewById(R.id.text_input_password_confirmation);
        firstNameEditText = findViewById(R.id.text_input_first_name);
        lastNameEditText = findViewById(R.id.text_input_last_name);
        errorMessageTextView = findViewById(R.id.text_sign_up_error);
        ageEditText = findViewById(R.id.text_input_age);
        locationEditText = findViewById(R.id.text_input_location);

        // set listener for profile picture image button
        // user should be able to choose and crop a profile picture
        profilePictureImageButton = findViewById(R.id.image_button_profile_picture);
        profilePictureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performProfilePictureSearch();
            }
        });

        // set listener for sign up button
        // application should verify the entered information and send it to the server
        Button signUpButton = findViewById(R.id.button_sign_up);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSignUp();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        passwordEditText.setText("");
        confirmPasswordEditText.setText("");
    }

    /**
     * Checks if the external storage on the device is readable
     *
     * @return true if external storage is readable
     */
    private boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state))
            return true;

        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
            return true;

        return false;
    }

    /**
     * Ask user for permission to read external storage and prompt for picture selection
     */
    private void performProfilePictureSearch() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

            // Permission to read external storage should be asked if not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show permission explanation if extra information is required about the external
                // storage permission. Show message through alert dialog
                AlertDialog.Builder permissionExplanationDialog = new AlertDialog.Builder(
                        getApplicationContext());
                permissionExplanationDialog.setMessage(
                        R.string.read_external_storage_permission_explanation);
                permissionExplanationDialog.setPositiveButton("Ok", null);
                permissionExplanationDialog.setCancelable(false);
                permissionExplanationDialog.create().show();
            }


            // Ask for permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

        }
        else {
            // prompt user to select picture from external storage
            askUserToSelectProfilePicture();
        }
    }

    /**
     * Prompt user to select picture from external storage
     */
    private void askUserToSelectProfilePicture() {
        if (isExternalStorageReadable()) {
            // Initiate intent to choose a picture from external storage
            Intent imageChooserIntent = CropImage.getPickImageChooserIntent(getApplicationContext());
            startActivityForResult(imageChooserIntent, CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
        }
        else {
            // Show message stating external storage is not readable
            Toast.makeText(getApplicationContext(), R.string.external_storage_not_readable,
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Prompt user to crop the selected profile picture
     *
     * @param profilePictureUri Uri for selected profile picture
     */
    private void performProfilePictureCrop(Uri profilePictureUri) {
        CropImage.ActivityBuilder profilePictureCropActivity = CropImage.activity(
                profilePictureUri);
        profilePictureCropActivity.setAutoZoomEnabled(true);
        profilePictureCropActivity.setFixAspectRatio(true);
        profilePictureCropActivity.setAspectRatio(1, 1);

        // set max size to profile picture
        profilePictureCropActivity.setMinCropResultSize(1000, 1000);
        profilePictureCropActivity.setMaxCropResultSize(1000, 1000);
        profilePictureCropActivity.start(this);
    }

    /**
     * Validate the entered account information.
     *
     * @param email           user's email
     * @param password        user's password
     * @param confirmPassword user's confirm password
     * @param firstName       person's first name
     * @param lastName        person's last name
     * @param age             person's age
     * @param location        person's location
     * @return Result of the validation.
     *         ACCOUNT_INFORMATION_VALIDATION_SUCCESS = 0;
     *         ACCOUNT_INFORMATION_VALIDATION_MISSING_INFO = 1;
     *         ACCOUNT_INFORMATION_VALIDATION_PASSWORD_MISMATCH = 2;
     */
    private byte validateAccountInformation(String email, String password, String confirmPassword, String firstName,
                                            String lastName, String age, String location) {

        // validation flags
        boolean missingAccountInformation = false;
        boolean passwordMismatch = false;
        boolean underage = false;

        // get error color code
        int errorColor = ContextCompat.getColor(getApplicationContext(), R.color.errorFontColor);
        // get hint color code
        int hintColor = ContextCompat.getColor(getApplicationContext(), R.color.colorHint);

        // if any field is empty change hint color to error color
        // and set missing account information flag to true
        // else set hint color to default color
        if (email.isEmpty()) {
            emailEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            emailEditText.setHintTextColor(hintColor);
        }

        if (password.isEmpty()) {
            passwordEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            passwordEditText.setHintTextColor(hintColor);
        }

        if (confirmPassword.isEmpty()) {
            confirmPasswordEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            confirmPasswordEditText.setHintTextColor(hintColor);
        }

        if (firstName.isEmpty()) {
            firstNameEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            firstNameEditText.setHintTextColor(hintColor);
        }

        if (lastName.isEmpty()) {
            lastNameEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            lastNameEditText.setHintTextColor(hintColor);
        }

        if (age.isEmpty()) {
            ageEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            ageEditText.setHintTextColor(hintColor);
        }

        if (location.isEmpty()) {
            locationEditText.setHintTextColor(errorColor);
            missingAccountInformation = true;
        }
        else {
            locationEditText.setHintTextColor(hintColor);
        }

        // if passwords don't match then set password mismatch flag to true
        if (!missingAccountInformation) {
            if (!password.equals(confirmPassword))
                passwordMismatch = true;
            else if (Integer.parseInt(age) < 18)
                underage = true;
        }

        // return the appropriate result code
        if (missingAccountInformation)
            return ACCOUNT_INFORMATION_VALIDATION_MISSING_INFO;
        else if(passwordMismatch)
            return ACCOUNT_INFORMATION_VALIDATION_PASSWORD_MISMATCH;
        else if (underage)
            return ACCOUNT_INFORMATION_VALIDATION_UNDERAGE;

        return ACCOUNT_INFORMATION_VALIDATION_SUCCESS;
    }

    /**
     * Perform sign up function
     * Validates all user information and shows appropriate error message
     * Or initiate sign up request
     */
    private void performSignUp() {
        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String confirmPassword = confirmPasswordEditText.getText().toString().trim();
        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();
        String ageStr = ageEditText.getText().toString().trim();
        String location = locationEditText.getText().toString().trim();

        // validate account information
        byte result = validateAccountInformation(email, password, confirmPassword, firstName,
                                                lastName, ageStr, location);

        // show appropriate error message
        if (result == ACCOUNT_INFORMATION_VALIDATION_MISSING_INFO)
            errorMessageTextView.setText(R.string.error_missing_information);
        else if (result == ACCOUNT_INFORMATION_VALIDATION_PASSWORD_MISMATCH)
            errorMessageTextView.setText(R.string.error_password_mismatch);
        else if (result == ACCOUNT_INFORMATION_VALIDATION_UNDERAGE)
            errorMessageTextView.setText(R.string.error_underage);
        else {
            errorMessageTextView.setText("");
            int age = Integer.parseInt(ageStr);
            sendSignUpRequest(email, password, new Person(email, firstName, lastName, age, "", location));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE
                && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                // ask user to crop the selected profile picture
                // get Uri of selected profile picture
                Uri profilePictureUri = resultData.getData();
                performProfilePictureCrop(profilePictureUri);
            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE
                && resultCode == Activity.RESULT_OK) {
            if (resultData != null) {
                // get Uri for cropped profile picture
                Uri croppedProfilePictureUri = CropImage.getActivityResult(resultData).getUri();

                // set cropped profile picture on image button
                profilePictureImageButton.setImageURI(croppedProfilePictureUri);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // if permission is granted ask the user to select a profile picture
                askUserToSelectProfilePicture();
            } else {
                // show message stating that the permission for external storage is denied
                Toast.makeText(getApplicationContext(), R.string.read_external_storage_permission_denied,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Call back function for successful sign up request
     * Start user type selection activity
     *
     * @param person person object
     */
    public void signUpSuccessfulCallback(final Person person) {
        Intent userTypeSelectionIntent = new Intent(this,
                UserTypeSelectionActivity.class);
        userTypeSelectionIntent.putExtra(IntentKeys.PERSON_KEY, person);
        startActivity(userTypeSelectionIntent);
        finish();
    }

    /**
     * Call back function for sign up request failures
     *
     * @param statusCode response status code
     */
    public void signUpFailureCallback(int statusCode) {
        // display the appropriate error message
        if (statusCode == ResponseVariables.CONFLICT_STATUS)
            errorMessageTextView.setText(getString(R.string.error_account_exists));
        else
            errorMessageTextView.setText(getString(R.string.error_account_failed));
    }

    /**
     * Send sign up request
     *
     * @param email    person email
     * @param password person password
     * @param person   person object
     */
    private void sendSignUpRequest(final String email, final String password, final Person person) {
        // serialize person object to request params
        RequestParams userAccountInformation = PersonSerializer.serialize(person);
        userAccountInformation.add(SerializerKeys.PASSWORD_KEY, password);

        TutorMeRestClient.post(Urls.SIGN_UP, userAccountInformation, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", new String(response)));
                signUpSuccessfulCallback(person);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", new String(errorResponse)));
                signUpFailureCallback(statusCode);
            }
        });
    }
}
