package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Person;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * PersonSerializer class handler JSON deserialization and Person
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class PersonSerializer {
    /**
     * Converts a person object to a request param object containing the person information
     * @param person person object to convert
     * @return request param object containing person information
     */
    public static RequestParams serialize(Person person) {
        RequestParams personInformation = new RequestParams();
        personInformation.put(SerializerKeys.EMAIL_KEY, person.email);
        personInformation.put(SerializerKeys.FIRST_NAME_KEY, person.firstName);
        personInformation.put(SerializerKeys.LAST_NAME_KEY, person.lastName);
        personInformation.put(SerializerKeys.AGE_KEY, person.age);
        personInformation.put(SerializerKeys.PROFILE_PIC_DIRECTORY, person.profilePictureDirectory);
        personInformation.put(SerializerKeys.LOCATION_KEY, person.location);

        return personInformation;
    }

    /**
     * Converts a JSON object containing person information to a Person object
     * @param personJSONObject JSON object containing person information
     * @return Person object containing person information
     */
    public static Person deserializer(JSONObject personJSONObject) {
        Person person = null;

        try {
            String email = personJSONObject.getString(SerializerKeys.EMAIL_KEY);
            String firstName = personJSONObject.getString(SerializerKeys.FIRST_NAME_KEY);
            String lastName = personJSONObject.getString(SerializerKeys.LAST_NAME_KEY);
            int age = personJSONObject.getInt(SerializerKeys.AGE_KEY);
            String profilePictureDirectory = personJSONObject.getString(SerializerKeys.PROFILE_PIC_DIRECTORY);
            String location = personJSONObject.getString(SerializerKeys.LOCATION_KEY);

            person = new Person(email, firstName, lastName, age, profilePictureDirectory, location);
        } catch (JSONException e) {
            Log.e("Person", e.getMessage());
        }

        return person;
    }
}
