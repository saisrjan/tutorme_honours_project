package com.example.tutorme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.listadapters.CourseListAdapter;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.CourseSerializer;
import com.example.tutorme.corelib.serializers.TutorSerializer;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * AddCourseActivity class allows the Tutor to view and add courses to their account
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class AddCourseActivity extends AppCompatActivity implements CourseListAdapterHandler {
    private static final String ERROR_TAG = "AddCourseActivity";

    private CourseListAdapter adapter;
    private Tutor tutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course);

        // add button should navigate the user to the course information activity to add a new course
        Button addCourseButton = findViewById(R.id.button_add_course);
        addCourseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startCourseInformationActivity();
            }
        });

        // done button should navigate the user to the home page activity
        Button doneButton = findViewById(R.id.button_add_course_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startHomePageActivity();
            }
        });

        // get tutor object from intent
        Intent intent = getIntent();
        tutor = (Tutor) intent.getSerializableExtra(IntentKeys.TUTOR_KEY);

        // inflate and initiate the course list and its adapter
        RecyclerView courseListRecyclerView = findViewById(R.id.list_view_courses);
        adapter = new CourseListAdapter(new ArrayList<Course>(), this);

        courseListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        courseListRecyclerView.setAdapter(adapter);
        courseListRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void onStart() {
        // get courses from database every time the activity refreshes
        sendGetCoursesRequest(tutor);
        super.onStart();
    }

    /**
     * Start course information activity for the purpose of adding a new course
     */
    private void startCourseInformationActivity() {
        Intent courseDetailsActivityIntent = new Intent(this,
                CourseInformationActivity.class);

        // add course mode is true for adding a new course
        courseDetailsActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.TUTOR);
        courseDetailsActivityIntent.putExtra(IntentKeys.ADD_COURSE_MODE, true);
        courseDetailsActivityIntent.putExtra(IntentKeys.TUTOR_KEY, tutor);
        startActivity(courseDetailsActivityIntent);
    }

    /**
     * Start a course information activity for the purpose of viewing an existing course information
     * and make any changes to it
     *
     * @param course course object to view
     */
    public void startCourseInformationActivity(final Course course) {
        Intent courseDetailsActivityIntent = new Intent(this, CourseInformationActivity.class);

        // add course mode is false for viewing and editing course information
        courseDetailsActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.TUTOR);
        courseDetailsActivityIntent.putExtra(IntentKeys.ADD_COURSE_MODE, false);
        courseDetailsActivityIntent.putExtra(IntentKeys.COURSE_KEY, course);
        startActivity(courseDetailsActivityIntent);
    }

    /**
     * Start homepage activity and pass the tutor object through the intent and close this activity
     */
    private void startHomePageActivity() {
        Intent homePageActivity = new Intent(this, HomePageActivity.class);
        homePageActivity.putExtra(UserType.USER_TYPE_KEY, UserType.TUTOR);
        homePageActivity.putExtra(IntentKeys.TUTOR_KEY, tutor);
        startActivity(homePageActivity);
        finish();
    }

    /**
     * Callback function for the the get courses response handler to update the course list
     * @param courseList list of course objects
     */
    private void updateCourseList(List<Course> courseList) {
        adapter.setCourseList(courseList);
    }

    /**
     * Sends a GET request to get courses associated with the tutor
     * @param tutor
     */
    private void sendGetCoursesRequest(final Tutor tutor) {
        // serialize the tutor object into request parameters
        // attributes are turned into key and value pairs
        RequestParams tutorInformation = TutorSerializer.serialize(tutor);

        TutorMeRestClient.get(Urls.GET_COURSES, tutorInformation, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i(ERROR_TAG, String.format("Received: %s", response));

                // parse JSON list into course objects
                List<Course> courseList = new ArrayList<>();
                try {
                    JSONArray courseJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < courseJSONArray.length(); i++) {
                        JSONObject courseJSONObject = courseJSONArray.getJSONObject(i);
                        courseList.add(CourseSerializer.deserialize(courseJSONObject));
                    }
                } catch (JSONException e) {
                    Log.e(ERROR_TAG,
                            String.format("Incorrect JSON response. Received: %s", response));
                }

                // update course list with new data set
                updateCourseList(courseList);
            }
        });
    }
}
