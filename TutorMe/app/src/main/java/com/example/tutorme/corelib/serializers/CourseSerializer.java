package com.example.tutorme.corelib.serializers;

import android.util.Log;

import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Tutor;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * CourseSerializer class handler JSON deserialization and Course
 * serialization
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class CourseSerializer {
    /**
     * Converts a course object to a request param object containing the course information
     * @param course course object to convert
     * @return request param object containing course information
     */
    public static RequestParams serialize(Course course) {
        RequestParams appointmentRequestParams = new RequestParams();
        appointmentRequestParams.put(SerializerKeys.EMAIL_KEY, course.tutor.email);
        appointmentRequestParams.put(SerializerKeys.CODE_KEY, course.code);
        appointmentRequestParams.put(SerializerKeys.NAME_KEY, course.name);
        appointmentRequestParams.put(SerializerKeys.DESCRIPTION_KEY, course.description);
        appointmentRequestParams.put(SerializerKeys.SCHOOL_KEY, course.school);
        appointmentRequestParams.put(SerializerKeys.GRADE_KEY, course.grade);

        return appointmentRequestParams;
    }

    /**
     * Converts a JSON object containing course information to a Course object
     * @param courseJSONObject JSON object containing course information
     * @return Course object containing course information
     */
    public static Course deserialize(JSONObject courseJSONObject) {
        Course course = null;

        try {
            JSONObject tutorJSONObject = courseJSONObject.getJSONObject(SerializerKeys.TUTOR_KEY);
            Tutor tutor = TutorSerializer.deserialize(tutorJSONObject);

            String code = courseJSONObject.getString(SerializerKeys.CODE_KEY);
            String name = courseJSONObject.getString(SerializerKeys.NAME_KEY);
            String description = courseJSONObject.getString(SerializerKeys.DESCRIPTION_KEY);
            String school = courseJSONObject.getString(SerializerKeys.SCHOOL_KEY);
            String grade = courseJSONObject.getString(SerializerKeys.GRADE_KEY);

            course = new Course(tutor, code, name, description, school, grade);
        } catch (JSONException e) {
            Log.e("CourseSerializer", e.getMessage());
        }

        return course;
    }
}
