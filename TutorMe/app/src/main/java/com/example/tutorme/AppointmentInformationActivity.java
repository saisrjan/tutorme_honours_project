package com.example.tutorme;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Appointment;
import com.example.tutorme.corelib.models.Course;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.AppointmentSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

/**
 * AppointmentInformationActivity class allows the user to book, view or edit the appointment
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class AppointmentInformationActivity extends AppCompatActivity {
    private static final String ERROR_TAG = "AppointmentInformation";

    private EditText recipientNameEditText;
    private EditText courseCodeEditText;
    private EditText courseNameEditText;
    private EditText dateEditText;
    private EditText timeEditText;
    private EditText durationEditText;
    private EditText priceEditText;
    private EditText locationEditText;
    private TextView errorTextView;

    private Student student;
    private Course course;
    private Appointment appointment;
    private byte userType;
    private boolean bookAppointmentMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_information);

        // inflate all activity components
        recipientNameEditText = findViewById(R.id.text_input_appointment_information_recipient_name);
        courseCodeEditText = findViewById(R.id.text_input_appointment_information_course_code);
        courseNameEditText = findViewById(R.id.text_input_appointment_information_course_name);
        dateEditText = findViewById(R.id.text_input_appointment_information_date);
        timeEditText = findViewById(R.id.text_input_appointment_information_time);
        durationEditText = findViewById(R.id.text_input_appointment_information_duration);
        priceEditText = findViewById(R.id.text_input_appointment_information_price);
        locationEditText = findViewById(R.id.text_input_appointment_information_location);
        errorTextView = findViewById(R.id.text_account_information_error);

        durationEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String durationStr = durationEditText.getText().toString();
                    if (!durationStr.isEmpty()) {
                        priceEditText.setText("$"+course.tutor.price * Integer.parseInt(durationStr));
                    }
                }
            }
        });

        Button bookAppointmentButton = findViewById(R.id.button_appointment_information_book_appointment);
        final Button acceptAppointmentButton = findViewById(R.id.button_appointment_information_accept_appointment);
        Button declineAppointmentButton = findViewById(R.id.button_appointment_information_decline_appointment);
        Button deleteAppointmentButton = findViewById(R.id.button_appointment_information_delete_appointment);

        // send create appointment request with the appointment information given
        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAppointment();
            }
        });

        // send accept appointment request
        acceptAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptAppointment();
            }
        });

        // send decline appointment request
        declineAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                declineAppointment();
            }
        });

        // send delete appointment request
        deleteAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAppointment();
            }
        });

        Intent intent = getIntent();
        // get user type and book appointment mode from intent
        userType = intent.getByteExtra(UserType.USER_TYPE_KEY, (byte) 0);
        bookAppointmentMode = intent.getBooleanExtra(IntentKeys.BOOK_APPOINTMENT_MODE, false);

        if (userType == UserType.STUDENT) {
            // disable accept, decline appointment options since they do not apply
            acceptAppointmentButton.setVisibility(View.INVISIBLE);
            declineAppointmentButton.setVisibility(View.INVISIBLE);

            if (bookAppointmentMode) {
                // get student, tutor and course objects from intent if a student is booking a new appointment
                student = (Student) intent.getSerializableExtra(IntentKeys.STUDENT_KEY);
                course = (Course) intent.getSerializableExtra(IntentKeys.COURSE_KEY);

                // disable delete appointment option
                bookAppointmentButton.setVisibility(View.VISIBLE);
                deleteAppointmentButton.setVisibility(View.INVISIBLE);
            }
            else {
                // get appointment object from intent
                appointment = (Appointment) intent.getSerializableExtra(IntentKeys.APPOINTMENT_KEYS);

                // disable book appointment option
                bookAppointmentButton.setVisibility(View.INVISIBLE);
                deleteAppointmentButton.setVisibility(View.VISIBLE);
            }
        }
        else {
            // get appointment object from intent
            appointment = (Appointment) intent.getSerializableExtra(IntentKeys.APPOINTMENT_KEYS);

            // book and delete appointment options are disables because they don't apply to tutor
            bookAppointmentButton.setVisibility(View.INVISIBLE);
            deleteAppointmentButton.setVisibility(View.INVISIBLE);

            // if appointment has not been accepted or declined then show those options
            if (!appointment.accepted && !appointment.declined) {
                acceptAppointmentButton.setVisibility(View.VISIBLE);
                declineAppointmentButton.setVisibility(View.VISIBLE);
            }
            else {
                acceptAppointmentButton.setVisibility(View.INVISIBLE);
                declineAppointmentButton.setVisibility(View.INVISIBLE);
            }
        }

        recipientNameEditText.setEnabled(false);
        courseCodeEditText.setEnabled(false);
        courseNameEditText.setEnabled(false);
        priceEditText.setEnabled(false);

        if (!bookAppointmentMode) {
            // if the user is not trying to book an appointment disable all input fields to prevent
            // change of data
            dateEditText.setEnabled(false);
            timeEditText.setEnabled(false);
            durationEditText.setEnabled(false);
            locationEditText.setEnabled(false);
        }

        // set appointment information
        setAppointmentInformation();
    }

    /**
     * Set appointment information
     */
    private void setAppointmentInformation() {
        String recipientName;
        if (bookAppointmentMode && userType == UserType.STUDENT) {
            // set tutor and course information only
            recipientName = course.tutor.firstName + " " + course.tutor.lastName;
            recipientNameEditText.setText(recipientName);
            courseCodeEditText.setText(course.code);
            courseNameEditText.setText(course.name);
        }
        else {
            // if appointment object is given through intent, user the information from it
            if (userType == UserType.STUDENT)
                recipientName = appointment.tutor.firstName + " " + appointment.tutor.lastName;
            else
                recipientName = appointment.student.firstName + " " + appointment.student.lastName;

            recipientNameEditText.setText(recipientName);
            courseCodeEditText.setText(appointment.course.code);
            courseNameEditText.setText(appointment.course.name);
            dateEditText.setText(appointment.date);
            timeEditText.setText(appointment.time);
            durationEditText.setText(String.format("%s", appointment.duration));
            priceEditText.setText(String.format("%s", appointment.price));

            locationEditText.setText(appointment.location);
        }
    }

    /**
     * Gets the appointment information from the activity, creates a new appointment object
     * and calls the sendCreateAppointmentRequest function to send the request with the information
     */
    private void createAppointment() {
        clearErrorMessage();

        String date = dateEditText.getText().toString();
        String time = timeEditText.getText().toString();
        String durationStr = durationEditText.getText().toString();
        String priceStr = priceEditText.getText().toString();
        String location = locationEditText.getText().toString();

        if (date.isEmpty() || time.isEmpty() || durationStr.isEmpty() || priceStr.isEmpty() ||
                location.isEmpty()) {
            // show missing information error if there is missing information
            showMissingInformationError();
        }
        else {
            // create appointment object
            appointment = new Appointment(student, course, date, time, Integer.parseInt(durationStr),
                    Integer.parseInt(priceStr.substring(1)), location, false, false);

            // send request
            sendCreateAppointmentRequest(appointment);
        }
    }

    private void acceptAppointment() {
        clearErrorMessage();
        appointment.accepted = true;
        appointment.declined = false;
        sendAcceptAppointmentRequest(appointment);
    }

    private void declineAppointment() {
        clearErrorMessage();
        appointment.accepted = false;
        appointment.declined = true;
        sendDeclineAppointmentRequest(appointment);
    }

    private void deleteAppointment() {
        clearErrorMessage();
        sendDeleteAppointmentRequest(appointment);
    }

    /**
     * Clears error message
     */
    private void clearErrorMessage() {
        errorTextView.setText("");
    }

    /**
     * Show missing error missing information
     */
    private void showMissingInformationError() {
        errorTextView.setText(R.string.error_missing_appointment_information);
    }

    /**
     * Appointment request successful callback
     */
    private void appointmentRequestSuccessfulCallback() {
        finish();
    }


    private void appointmentDeleteRequestSuccssfulCallback() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Would you like to write a review for your tutor?");
        // Add the buttons
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent reviewInformationActivityIntent = new Intent(AppointmentInformationActivity.this, ReviewInformationActivity.class);
                reviewInformationActivityIntent.putExtra(UserType.USER_TYPE_KEY, UserType.STUDENT);
                reviewInformationActivityIntent.putExtra(IntentKeys.WRITE_REVIEW_MODE, true);
                reviewInformationActivityIntent.putExtra(IntentKeys.PERSON_REVIEWER_KEY, appointment.student);
                reviewInformationActivityIntent.putExtra(IntentKeys.PERSON_REVIEWEE_KEY, appointment.tutor);
                reviewInformationActivityIntent.putExtra(IntentKeys.COURSE_KEY, appointment.course);
                startActivity(reviewInformationActivityIntent);

                finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Callback function for create appointment failure
     *
     * @param status response status code
     */
    private void createAppointmentRequestFailureCallback(int status) {
        // display appropriate error message
        if (status == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_create_appointment_exists);
        else
            errorTextView.setText(R.string.error_create_appointment_failed);
    }

    /**
     * Callback function for accept appointment failure
     *
     * @param status response status code
     */
    private void acceptAppointmentRequestFailureCallback(int status) {
        // display the appropriate error message
        if (status == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_accept_appointment_does_not_exist);
        else
            errorTextView.setText(R.string.error_accept_appointment_failed);
    }

    /**
     * Callback function for decline appointment failure
     * @param status response status code
     */
    private void declineAppointmentRequestFailureCallback(int status) {
        // display the appropriate error message
        if (status == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_decline_appointment_does_not_exist);
        else
            errorTextView.setText(R.string.error_decline_appointment_failed);
    }

    /**
     * Callback function for delete appointment failure
     *
     * @param status response status code
     */
    private void deleteAppointmentRequestFailureCallback(int status) {
        // display the appropriate error message
        if (status == ResponseVariables.CONFLICT_STATUS)
            errorTextView.setText(R.string.error_delete_appointment_does_not_exist);
        else
            errorTextView.setText(R.string.error_delete_appointment_failed);
    }

    /**
     * Send create appointment request
     *
     * @param appointment appointment object containing appointment information
     */
    private void sendCreateAppointmentRequest(final Appointment appointment) {
        // serialize appointment object to request param object containing appointment information
        // each attribute is a (key, value) pair
        RequestParams appointInformationRequestParams = AppointmentSerializer.serialize(appointment);

        TutorMeRestClient.post(Urls.CREATE_APPOINTMENT, appointInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                appointmentRequestSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                createAppointmentRequestFailureCallback(statusCode);
            }
        });
    }

    /**
     * Send accept appointment request
     *
     * @param appointment appointment object containing appointment information
     */
    private void sendAcceptAppointmentRequest(final Appointment appointment) {
        // serialize appointment object to request param object containing appointment information
        // each attribute is a (key, value) pair
        RequestParams appointInformationRequestParams = AppointmentSerializer.serialize(appointment);

        TutorMeRestClient.post(Urls.UPDATE_APPOINTMENT, appointInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                appointmentRequestSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                acceptAppointmentRequestFailureCallback(statusCode);
            }
        });
    }

    /**
     * Sends decline appointment request
     *
     * @param appointment appointment object containing appointment information
     */
    private void sendDeclineAppointmentRequest(final Appointment appointment) {
        // serialize appointment object to request param object containing appointment information
        // each attribute is a (key, value) pair
        RequestParams appointInformationRequestParams = AppointmentSerializer.serialize(appointment);

        TutorMeRestClient.post(Urls.UPDATE_APPOINTMENT, appointInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                appointmentRequestSuccessfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                declineAppointmentRequestFailureCallback(statusCode);
            }
        });
    }

    /**
     * Sends delete appointment request
     *
     * @param appointment appointment object containing appointment information
     */
    private void sendDeleteAppointmentRequest(final Appointment appointment) {
        // serialize appointment object to request param object containing appointment information
        // each attribute is a (key, value) pair
        RequestParams appointInformationRequestParams = AppointmentSerializer.serialize(appointment);

        TutorMeRestClient.post(Urls.DELETE_APPOINTMENT, appointInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                appointmentDeleteRequestSuccssfulCallback();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                deleteAppointmentRequestFailureCallback(statusCode);
            }
        });
    }
}
