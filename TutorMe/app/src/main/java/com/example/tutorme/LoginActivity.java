package com.example.tutorme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.networking.ResponseVariables;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.SerializerKeys;
import com.example.tutorme.corelib.serializers.StudentSerializer;
import com.example.tutorme.corelib.serializers.TutorSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.cookie.Cookie;

/**
 * LoginActivity class allows the user to login
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class LoginActivity extends AppCompatActivity {
    private static final String ERROR_TAG = "LoginActivity";

    private EditText emailEditText;
    private EditText passwordEditText;
    private TextView errorTextView;

    private String email;
    private String password;

    private byte userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // set the user type for this login activity
        Intent loginIntent = getIntent();
        userType = loginIntent.getByteExtra(UserType.USER_TYPE_KEY, (byte) 0);

        // set title of the login page depending on the user type
        TextView loginTitle = findViewById(R.id.text_login_title);
        if (userType == UserType.STUDENT)
            loginTitle.setText(R.string.student_login_title);
        else if ((userType == UserType.TUTOR))
            loginTitle.setText(R.string.tutor_login_title);
        else
            loginTitle.setText(R.string.login_title);

        // inflate the login activity components
        emailEditText = findViewById(R.id.text_input_login_email);
        passwordEditText = findViewById(R.id.text_input_login_password);
        errorTextView = findViewById(R.id.text_login_error);

        // set login button action
        Button loginButton = findViewById(R.id.button_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    @Override
    protected void onPause() {
        // clear all sensitive credentials from memory
        clearCredentials();
        super.onPause();
    }

    @Override
    protected void onStop() {
        // clear all sensitive credentials from memory
        clearCredentials();
        super.onStop();
    }

    /**
     * Clear all sensitive credentials from memory
     */
    private void clearCredentials() {
        passwordEditText.setText("");

        email = null;
        password = null;
    }

    /**
     * Sends a login request
     * Tutor login is performed when login is for a tutor.
     * Student login is performed when login is for a student.
     */
    private void login() {
        // get email and password input
        email = emailEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();

        // clear existing error messages
        errorTextView.setText("");

        // perform login REST API call
        if (userType == UserType.STUDENT)
            // student login request
            sendStudentLoginRequest(email, password);
        else
            // tutor login request
            sendTutorLoginRequest(email, password);
    }

    /**
     * Creates an intent to start HomeActivity for a student user type.
     * And closes the LoginActivity.
     */
    private void startStudentHomePageActivity(Student student) {
        Intent studentHomeActivityIntent = new Intent(this, HomePageActivity.class);

        // forward email and user type to the HomeActivity
        studentHomeActivityIntent.putExtra(UserType.USER_TYPE_KEY, userType);
        studentHomeActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);

        startActivity(studentHomeActivityIntent);
        finish();
    }

    /**
     * Creates an intent to start HomeActivity for a tutor user type.
     * And closes the LoginActivity.
     */
    private void startTutorHomePageActivity(Tutor tutor) {
        Intent tutorHomeActivityIntent = new Intent(this, HomePageActivity.class);

        // forward email and user type to the HomeActivity
        tutorHomeActivityIntent.putExtra(UserType.USER_TYPE_KEY,  userType);
        tutorHomeActivityIntent.putExtra(IntentKeys.TUTOR_KEY, tutor);

        startActivity(tutorHomeActivityIntent);
        finish();
    }

    /**
     * Sets the appropriate error message based on the status code given
     *
     * @param status error status code received from the server
     */
    private void loginFailure(int status) {
        if (status == ResponseVariables.CONFLICT_STATUS) {
            // show error message stating that an account with the specified credentials does not exist
            errorTextView.setText(R.string.error_login_account_does_not_exist);
        }
        else {
            // show error message stating that an error occurred while logging in.
            errorTextView.setText(R.string.error_login_failed);
        }
    }

    /**
     * Send get student request
     *
     * @param email email of student
     */
    private void sendGetStudentRequest(final String email) {
        // create request parameters object with credential information
        RequestParams studentInformationRequestParams = new RequestParams();
        studentInformationRequestParams.put(SerializerKeys.EMAIL_KEY, email);

        // make get student request
        TutorMeRestClient.get(Urls.GET_STUDENT, studentInformationRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i(ERROR_TAG, String.format("Received: %s", response));
                try {
                    JSONObject studentJSONObject = response.getJSONObject("data");

                    // deserialize student JSON object to student object
                    Student student = StudentSerializer.deserializer(studentJSONObject);
                    startStudentHomePageActivity(student);

                } catch (JSONException e) {
                    Log.e(ERROR_TAG, e.getMessage());
                }
            }
        });
    }

    /**
     * Send get tutor request
     *
     * @param email email of tutor
     */
    private void sendGetTutorRequest(final String email) {
        // create request parameters object with credential information
        RequestParams tutorInformationRequestParams = new RequestParams();
        tutorInformationRequestParams.put(SerializerKeys.EMAIL_KEY, email);

        // make get tutor request
        TutorMeRestClient.get(Urls.GET_TUTOR, tutorInformationRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i(ERROR_TAG, String.format("Received: %s", response));
                try {
                    JSONObject tutorJSONObject = response.getJSONObject("data");

                    // deserialize tutor JSON object to tutor object
                    Tutor tutor = TutorSerializer.deserialize(tutorJSONObject);
                    startTutorHomePageActivity(tutor);

                } catch (JSONException e) {
                    Log.e(ERROR_TAG, e.getMessage());
                }
            }
        });
    }


    /**
     * Makes a login REST API call for student account
     *
     * @param email         account's email
     * @param password      account's password
     */
    public void sendStudentLoginRequest(final String email, final String password) {
        // create request parameters object with credential information
        RequestParams studentInformation = new RequestParams();
        studentInformation.put(SerializerKeys.EMAIL_KEY, email);
        studentInformation.put(SerializerKeys.PASSWORD_KEY, password);

        // make login request
        TutorMeRestClient.post(Urls.STUDENT_LOGIN, studentInformation, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                sendGetStudentRequest(email);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                loginFailure(statusCode);
            }
        });
    }

    /**
     * Makes a login REST API call for tutor account
     *
     * @param email         account's email
     * @param password      account's password
     */
    public void sendTutorLoginRequest(final String email, final String password) {
        // create request parameters object with credential information
        RequestParams tutorInformation = new RequestParams();
        tutorInformation.put(SerializerKeys.EMAIL_KEY, email);
        tutorInformation.put(SerializerKeys.PASSWORD_KEY, password);

        // make login request
        TutorMeRestClient.post(Urls.TUTOR_LOGIN, tutorInformation, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                Cookie csrfToken = null;
                for (Cookie c : MainActivity.myCookieStore.getCookies()) {
                    if (c.getName().equals("csrftoken")) {
                        csrfToken = c;
                        break;
                    }
                }

                if (csrfToken != null)
                    TutorMeRestClient.getClient().addHeader("X-CSRFToken", csrfToken.getValue());

                sendGetTutorRequest(email);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                loginFailure(statusCode);

                for (Cookie c : MainActivity.myCookieStore.getCookies()) {
                    Log.i("DEBUG", c.toString());
                }
            }
        });
    }
}

