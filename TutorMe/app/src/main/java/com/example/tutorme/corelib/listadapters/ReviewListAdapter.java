package com.example.tutorme.corelib.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tutorme.R;
import com.example.tutorme.TutorInformationActivity;
import com.example.tutorme.corelib.models.Review;

import java.util.List;

/**
 * ReviewListAdapter class handles view creation and data set changes
 * for the Recycler View
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.ReviewViewHolder> {
    private List<Review> reviewList;
    private TutorInformationActivity tutorInformationActivity;

    public class ReviewViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public ReviewViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public ReviewListAdapter(List<Review> reviewList, TutorInformationActivity tutorInformationActivity) {
        this.reviewList = reviewList;
        this.tutorInformationActivity = tutorInformationActivity;
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_item,
                parent, false);

        viewItem.setClickable(true);
        return new ReviewViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder courseViewHolder, int position) {
        final Review review = reviewList.get(position);

        // inflate all the components of the review list item
        TextView studentNameTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_review_student_name);
        TextView ratingTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_review_rating);
        TextView reviewDescriptionTextView = courseViewHolder.itemView.findViewById(R.id.text_list_item_review_description);

        // set values to the appropriate views
        String studentName = review.reviewer.firstName + " " + review.reviewer.lastName;
        studentNameTextView.setText(studentName);

        String rating = "Rating: " + review.rating;
        ratingTextView.setText(rating);

        reviewDescriptionTextView.setText(review.reviewText);

        courseViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // callback function to start review information activity
                tutorInformationActivity.startReviewInformationActivity(review);
            }
        });
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    /**
     * Update review data and notify the list view of the data change
     *
     * @param reviewList list of review objects
     */
    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
        notifyDataSetChanged();
    }
}
