package com.example.tutorme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.tutorme.corelib.IntentKeys;
import com.example.tutorme.corelib.models.Appointment;
import com.example.tutorme.corelib.listadapters.AppointmentListAdapter;
import com.example.tutorme.corelib.networking.TutorMeRestClient;
import com.example.tutorme.corelib.UserType;
import com.example.tutorme.corelib.models.Person;
import com.example.tutorme.corelib.models.Student;
import com.example.tutorme.corelib.models.Tutor;
import com.example.tutorme.corelib.networking.Urls;
import com.example.tutorme.corelib.serializers.AppointmentSerializer;
import com.example.tutorme.corelib.serializers.PersonSerializer;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * HomePageActivity class allows the user view upcoming appointments
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class HomePageActivity extends AppCompatActivity {
    public static final String ERROR_TAG = "HomePageActivity";

    private AppointmentListAdapter adapter;

    private byte userType;
    private Tutor tutor;
    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        // get user type from intent
        userType = intent.getByteExtra(UserType.USER_TYPE_KEY, (byte) 0);

        // get student or tutor object from intent depending on the intent
        if (userType == UserType.STUDENT)
            student = (Student) intent.getSerializableExtra(IntentKeys.STUDENT_KEY);
        else
            tutor = (Tutor) intent.getSerializableExtra(IntentKeys.TUTOR_KEY);

        // appointment list
        RecyclerView appointmentListRecyclerView = findViewById(R.id.list_view_appointments);
        adapter = new AppointmentListAdapter(new ArrayList<Appointment>(), this);

        appointmentListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        appointmentListRecyclerView.setAdapter(adapter);
        appointmentListRecyclerView.setHasFixedSize(true);
    }

    @Override
    protected void onStart() {
        sendGetAppointmentsRequest();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // only provide option to search for tutors if the user is a student
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.tutor_search) {
            // start tutor search activity
            Intent tutorSearchActivityIntent = new Intent(this, TutorSearchActivity.class);
            tutorSearchActivityIntent.putExtra(IntentKeys.STUDENT_KEY, student);
            startActivity(tutorSearchActivityIntent);
        }
        else if (id == R.id.button_logout) {
            // start main activity
            Person person;
            if (userType == UserType.STUDENT)
                person = student;
            else
                person = tutor;

            sendLogoutRequest(person);
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Callback function to update appointment list
     * @param appointments
     */
    private void updateAppointmentList(List<Appointment> appointments) {
        adapter.setAppointmentList(appointments);
    }


    /**
     * Send get appointment request
     */
    private void sendGetAppointmentsRequest() {
        String getURL;
        Person person;
        if (userType == UserType.STUDENT) {
            getURL = Urls.GET_APPOINTMENT_FOR_STUDENT;
            person = student;
        }
        else {
            getURL = Urls.GET_APPOINTMENT_FOR_TUTOR;
            person = tutor;
        }

        // serializer person object to request params object
        RequestParams personInformationRequestParams = PersonSerializer.serialize(person);

        TutorMeRestClient.get(getURL, personInformationRequestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.i(ERROR_TAG, String.format("Received: %s", response));

                try {
                    JSONArray appointmentArray = response.getJSONArray("data");
                    List<Appointment> appointmentList = new ArrayList<>();
                    for (int i = 0; i < appointmentArray.length(); i++) {
                        JSONObject appointmentJSONObject = appointmentArray.getJSONObject(i);

                        // deserializer JSON data to appointment object
                        appointmentList.add(AppointmentSerializer.deserialize(appointmentJSONObject));
                    }

                    // update appointment list
                    updateAppointmentList(appointmentList);

                } catch (JSONException e) {
                    Log.e(ERROR_TAG, e.getMessage());
                    return;
                }
            }
        });
    }

    /**
     * Send get appointment request
     */
    private void sendLogoutRequest(final Person person) {
        // serializer person object to request params object
        RequestParams personInformationRequestParams = PersonSerializer.serialize(person);

        TutorMeRestClient.post(Urls.LOGOUT_USER, personInformationRequestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
                startMainActivity();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Log.i(ERROR_TAG, String.format("Received: %s", statusCode));
            }
        });
    }

    /**
     * Callback function to start appointment information activity
     *
     * @param appointment appointment object
     */
    public void startAppointmentInformationActivity(Appointment appointment) {
        Intent appointmentInformationActivity = new Intent(this, AppointmentInformationActivity.class);
        appointmentInformationActivity.putExtra(UserType.USER_TYPE_KEY, userType);
        appointmentInformationActivity.putExtra(IntentKeys.BOOK_APPOINTMENT_MODE, false);
        appointmentInformationActivity.putExtra(IntentKeys.APPOINTMENT_KEYS, appointment);
        startActivity(appointmentInformationActivity);
    }

    public void startMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        startActivity(mainActivityIntent);
        finish();
    }
}
