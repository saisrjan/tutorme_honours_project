package com.example.tutorme.corelib;

/**
 * UserType class contains the variables used to determine user type
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class UserType {
    public static final String USER_TYPE_KEY = "user_type";
    public static final byte STUDENT = 0;
    public static final byte TUTOR = 1;
}
