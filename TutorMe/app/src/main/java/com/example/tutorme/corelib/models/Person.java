package com.example.tutorme.corelib.models;

import java.io.Serializable;

/**
 * Person class is a model for the person JSON object
 *
 * @author  Sai Janjanam
 * @version 1.0
 * @since   2019-04-20
 */
public class Person implements Serializable {
    public String email;
    public String firstName;
    public String lastName;
    public int age;
    public String profilePictureDirectory;
    public String location;

    public Person(String email, String firstName, String lastName, int age, String profilePictureDirectory,
                  String location) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.profilePictureDirectory = profilePictureDirectory;
        this.location = location;
    }
}
